![Raspi Logo](https://sourceforge.net/p/octave-raspberrypi/code/ci/default/tree/doc/raspi-logo.png?format=raw)

Introduction
============

This is a basic implementation of the Matlab raspberrypiio toolkit extension.

It attempts to provide the same function calls as the Matlab toolkit, as well as additional functionality, HOWEVER,
is not binary compatible with matlab. (ie: you must configure the Raspberry Pi correctly in order communicate with this
toolkit.

Requirements
============

The toolkit requires the instrument-control toolkit for TCP functionality.

The raspberry Pi should have the pigpio daemon installed and running.

Installing
==========

To install, run the octave package manager:

1. to install from source forge:
    pkg install https://sourceforge.net/projects/octave-raspberrypi/files/vXXXXXX/raspi-XXXXXX.tar.gz/download

2. to install from a local tarball.
    pkg install raspi-XXXXXXX.tar.gz

Where XXXXXXX is the version of the the downloaded tarball.

Usage:
======

1. Load the raspi package.
    pkg load raspi


2. Open a connection to the raspi using dns resolvable name or ip.
   a = raspi ('raspberrypi.local')

4. Use the raspi function calls to control hardware.
See the function list and examples directories.

Working Board Versions
===============================

The toolkit should work on the following boards, others may also work:

 * Raspberry Pi Rev 1
 * Raspberry Pi 2
 * Raspberry Pi 3
 * Raspberry Pi 4
 * Raspberry Pi Zero
 * Raspberry Pi Zero 2

Known limitations and bugs
==========================

