#!/bin/sh

## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## Install script for setup/configation of a Raspbeery Pi for the
## GNU Octave raspi toolkit.

SCRIPTFILE=$0

# running as sudo/root
if [ "`id -u`" != "0" ]; then
  echo "Not running as root or sudo"
  exit 1
fi

# verify is a pi
PI=`cat /proc/cpuinfo | grep 'Raspberry Pi'`
if [ -z "$PI" ]; then
  echo "Didnt detect as a Raspberry PI - not installing."
  exit 1
fi

echo "Running raspi_setup_hw - 0.0.1"
CWD=`pwd`
TMP=/tmp/raspi_setup-`date +%s`

match=$(grep --text --line-number '^PAYLOAD:$' $SCRIPTFILE | cut -d ':' -f 1)
payload_start=$((match + 1 ))

error=0

if [ "$match" = "" ]; then
  error=1
  echo "ERROR: could not find install data in installer"
fi

if [ "$check$error" = "10" ]; then
  echo -n "Checking data integrity ..."
  checksum=$(grep --text '^CHECKSUM: ' $SCRIPTFILE |  awk '{ print $2 }')
  vchecksum=$(tail -n +$payload_start $SCRIPTFILE | sha1sum -b | awk '{ print $1 }')

  if [ "$checksum" = "$vchecksum" ]; then
    echo "OK"
  else
    echo "FAILED"
    error=1
  fi
fi

# error occured
if [ "$error" = "1" ]; then
  exit 1
fi

mkdir $TMP

echo "Unpacking files ..."
tail -n +$payload_start $SCRIPTFILE | tar -C $TMP -xzf -

echo "Running script ... "
cd $TMP/
chmod a+rx raspi_setup.sh
chmod a+rx cgi/raspi_*
./raspi_setup.sh
error=$?

echo "Cleaning up ... "
#rm -rf $TMP

cd $CWD

echo "Done"

exit 0

CHECKSUM: 47cc1045f4a912a19673d5477e49b5972c676df8
PAYLOAD:
�     �Xmo�6�g���b mY��$@�dK�6o]8	��Z�-.���T2���X����t�L �%�=���=<:ɰ����c{{�>[ۛ���t�Z�vk�����h՚�&>�`��#7�i����>6��_3��߸H�C�����V�sg�ۛ���ollc�7:�f���?�Xyd����ނ__�!H,���`c�z "����JG���Р�����4��D�7��
c!B�U��1~���g;��<�q�x�,0�b�'J�
^� ��:�z^m9����X1~(����qn���[[�����/��?+���nb9����i�����!/�8+�-�t�{&�
�x�8�o�͇D�U��/Si�����7���F����G>�_��\&AB,�0J�1a"����el�U�������ꥇ�,vR�
o�4t@�P'��ՔL���XDW)U�(�h4��e�{'G��ˈ���ҡ�r4B"z#-2�y[���տD�e�_Hg�h~�Ц�v�qf��+GD&� �M;@ؽ� ǀEO3E��D/�9�B��4���ˬ�ѯ�W��Vsc��񾸬����͒�z��6�ʇjI��R�P������|�YQ{��_G�}��9��x�N��6�̱�B��u%�ݿRe�*O�,��eo���O�<k����g�����m��������
�l���?�v�݄U�J�(�|�;>6x���B4��2����@��H�1�ס�[�QC{ôx�C�RЂK��9ȭ $���E�[9���BBB�G�ě�:r�����E*4K�4$2�W2��pizcb����@H�l8+m�#b:f�J�AH��p-��gؘ�Q����O"�O������H��Um����MN�0��*C�b�D�od��@@n�0O�%	�u�����9일��{������(lc�[ע���,����i��	�O�?��@����W��7�u�O���ׇ=8��w�/^������;;l �	2K�={<ta�}��2��[��`h��p�ٵ�GB^�q"̪���@XB=��³�D�CH�]���6�63�����1JsJ�0)�L��p����7y�)m�DZf�9�tg��ga�ҡ�����8��;#�i9��Tr���v�O���n'Ĥ39ʤ�EA�i갌;�C��-[�K<g��҇'�w�_C+�I�#������s�b��L�9e���Q��t����*y�M�soh��贻hŁ䩥�ȒŦ��](��ȚI��6L+�0��%:d,�b#a�b��{E�v+J�A�c�F�B`��E0��)|��ݎ
Zz�bp&�X�Xd�1q`�ƽr�q�)�.M9L���[Dw��J�Z��ctpw�o���ԝ��v�,�|�.���}��&��F�2o10���ddȢ��Y�/�����U�ߦs�pYɋb�V]p�9mvw<x�0Vc�f�kY�X�"��kPc������O]��q�Z���;�P�_�(t�V3�A���a^��Q-Pr"ga��1�W��{|�H?aL��e�4��x��el�;~d\	�T|q��[�\2�P*IӐUմ�_</ʀ��Uw���̌YJ�L�2��.c��6&��D�r_<t�yT��́��@�D1��|��K���A�9%������4Ǐy�J����@��[��-�Ĺ+cif������.<�T-v�?�?���Ee9�c9�c9�c9����	�4 (  