#!/bin/sh

## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## Install script for setup/configation of a Raspbeery Pi for the
## GNU Octave raspi toolkit.

SCRIPTFILE=$0

# running as sudo/root
if [ "`id -u`" != "0" ]; then
  echo "Not running as root or sudo"
  exit 1
fi

# verify is a pi
PI=`cat /proc/cpuinfo | grep 'Raspberry Pi'`
if [ -z "$PI" ]; then
  echo "Didnt detect as a Raspberry PI - not installing."
  exit 1
fi

echo "Running raspi_setup_hw - %VERSION%"
CWD=`pwd`
TMP=/tmp/raspi_setup-`date +%s`

match=$(grep --text --line-number '^PAYLOAD:$' $SCRIPTFILE | cut -d ':' -f 1)
payload_start=$((match + 1 ))

error=0

if [ "$match" = "" ]; then
  error=1
  echo "ERROR: could not find install data in installer"
fi

if [ "$check$error" = "10" ]; then
  echo -n "Checking data integrity ..."
  checksum=$(grep --text '^CHECKSUM: ' $SCRIPTFILE |  awk '{ print $2 }')
  vchecksum=$(tail -n +$payload_start $SCRIPTFILE | sha1sum -b | awk '{ print $1 }')

  if [ "$checksum" = "$vchecksum" ]; then
    echo "OK"
  else
    echo "FAILED"
    error=1
  fi
fi

# error occured
if [ "$error" = "1" ]; then
  exit 1
fi

mkdir $TMP

echo "Unpacking files ..."
tail -n +$payload_start $SCRIPTFILE | tar -C $TMP -xzf -

echo "Running script ... "
cd $TMP/
chmod a+rx raspi_setup.sh
chmod a+rx cgi/raspi_*
./raspi_setup.sh
error=$?

echo "Cleaning up ... "
#rm -rf $TMP

cd $CWD

echo "Done"

exit 0

