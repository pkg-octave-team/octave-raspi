#!/bin/sh

## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## Create the raspi_setup_hw.sh

VERSION=0.0.1
TAR=tar
TAR_REPRODUCIBLE_OPTIONS="--sort=name --mtime=2000-01-01 --owner=0 --group=0 --numeric-owner"
TAR_OPTIONS="--format=ustar $TAR_REPRODUCIBLE_OPTIONS"

echo "*** Creating raspi_setup_hw.sh ..."
cat raspi_setup_hw.sh.in | sed "s/%VERSION%/$VERSION/g" > raspi_setup_hw.sh

$TAR cf - cgi raspi_setup.sh | gzip -9n > data.tgz
CHECKSUM=`sha1sum -b data.tgz | awk '{ print $1 }'`
echo "CHECKSUM: $CHECKSUM" >> raspi_setup_hw.sh
echo "PAYLOAD:" >> raspi_setup_hw.sh
cat data.tgz >> raspi_setup_hw.sh
chmod a+rx raspi_setup_hw.sh

# remove tmp stuff
echo "*** Removing temp files ..."
rm data.tgz

echo "*** done"
