#!/bin/sh

## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## Basic support script for setting up/configuring a Raspeery Pi OS for
## the raspi toolkit

PIGPIODIR=/opt/pigpio

# running as sudo/root
if [ "`id -u`" != "0" ]; then
  echo "Not running as root or sudo"
  exit 1
fi

# verify is a pi
PI=`cat /proc/cpuinfo | grep 'Raspberry Pi'`
if [ -z "$PI" ]; then
  echo "Didnt detect as a Raspberry PI - not installing."
  exit 1
fi

# install required packages
echo "Checking for and installing packages ..."
apt-get --assume-yes install pigpiod i2c-tools avahi-daemon openssh-server

# raspi-config
echo "Enabling raspi configurations ..."
raspi-config  nonint do_ssh 0
raspi-config  nonint do_spi 0
raspi-config  nonint do_i2c 0
raspi-config  nonint do_serial 2
raspi-config  nonint do_rgpio 0

# configuration adds
# /opt/pigpio/access
if [ -z "`grep 'raspi toolkit' $PIGPIODIR/access`" ]; then
 echo "# added for raspi toolkit" >> $PIGPIODIR/access
 if [ -z "`grep '/home/pi/*' $PIGPIODIR/access`" ]; then
  echo '/home/pi/* u' >> $PIGPIODIR/access
 fi
 if [ -z "`grep '/tmp/raspi-*' $PIGPIODIR/access`" ]; then
  echo '/tmp/raspi-* u' >> $PIGPIODIR/access
 fi
 echo '* r' >> $PIGPIODIR/access
fi
# /etc/avahi/avahi-daemon.conf

# ensure the services are marked to starting on boot
echo "Enabling pigpiod and avahi-daemon ..."
systemctl enable pigpiod
systemctl enable avahi-daemon

# install the pigpio scripts to /opt/pigpio/cgi/

cp cgi/* $PIGPIODIR/cgi/

# changes for the pigpiod conf ?
# /etc/systemd/system/pigpiod.service.d/public.conf

systemctl daemon-reload

echo "--------------------------------------"
echo "TODO: additional manual configurations"
echo "* change the pi name by changing /etc/hostname"
echo "* disable GUI (if enabled)"
echo "--------------------------------------"
