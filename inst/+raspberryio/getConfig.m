## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} raspberryio.getConfig (@var{id})
## Undocumented helper function
## @end deftypefn

function config = getConfig(id)
   # pi 2 and on use bits of the id to describe it
   # NOQu uuWu FMMM CCCC PPPP TTTT TTTT RRRR
   # from https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#raspberry-pi-revision-codes
   # for older pis, need to work out from id value
   switch (id)
     case { 2, 3 }
       conf = 1;
       rev = "1.0";
       typeval = 1;
     case { 4, 5, 6 }
       conf = 2;
       rev = "2.0";
       typeval = 1;
     case { 7, 8, 9}
       typeval = 0;
       rev = "2.0";
       conf = 2;
     case { 0xd, 0xe, 0xf}
       typeval = 1;
       rev = "2.0";
       conf = 2;
     case { 0x10}
       typeval = 3;
       rev = "1.2";
       conf = 3;
     case {0x11}
       typeval = 6;
       rev = "1.0";
       conf = 3;
     case {0x12}
       typeval = 2;
       rev = "1.1";
       conf = 3;
     case {0x13}
       typeval = 3;
       rev = "1.2";
       conf = 3;
     case {0x14}
       typeval = 6;
       rev = "1.0";
       conf = 3;
     case {0x15}
       typeval = 2;
       rev = "1.1";
       conf = 3;
     otherwise
       # NOQuuuWuFMMMCCCCPPPPTTTTTTTTRRRR
       conf = 3;
       rev = ["1." num2str(bitand(id, int32(0xf)))];
       typeval = bitand(bitshift(id, -4), int32(0xff));
   endswitch

   switch typeval
     case 0, type=""; model= "A";
     case 1, type=""; model= "B";
     case 2, type=""; model= "A+";
     case 3, type=""; model= "B+";
     case 4, type=" 2"; model= "B";
     case 5, type=""; model= "Alpha";
     case 6, type=" 1"; model= "CM";
     case 8, type=" 3"; model= "B";
     case 9, type=""; model= "Zero";
     case 10, type=" 3"; model= "CM";
     case 12, type=""; model= "Zero W";
     case 13, type=" 3"; model= "B+";
     case 14, type=" 3"; model= "A+";
     case 16, type=" 3"; model= "CM+";
     case 17, type=" 4"; model= "B";
     case 18, type=""; model= "Zero 2 W";
     case 19, type=" 400"; model= "";
     case 20, type=" 4"; model= "CM";
     otherwise
       model = "unknown";
       rev = sprintf("%d", id);
       type = "";
   endswitch

   # load config file
   config = eval(sprintf("raspberryio.config.config_%d(%d)", conf, id));
   config.model = model;
   config.type = type;
   config.hwver = id;

   config.boardname = sprintf("Raspberry Pi%s Model %s Rev %s", type, model, rev);

endfunction
