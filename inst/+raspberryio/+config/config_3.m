function config = config_3 (id)
  config = [];
  config.pins = {};
  # User GPIO GPIO 2-27 (0 and 1 are reserved)
  config.pins{end+1} = raspberryio.config.pin_info(0, {"reserved"});
  config.pins{end+1} = raspberryio.config.pin_info(1, {"reserved"});
  config.pins{end+1} = raspberryio.config.pin_info(2, {"digital", "pwm", "i2c1_sda"});
  config.pins{end+1} = raspberryio.config.pin_info(3, {"digital", "pwm", "i2c1_scl"});
  config.pins{end+1} = raspberryio.config.pin_info(4, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(5, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(6, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(7, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(8, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(9, {"digital", "pwm", "spi_miso"});
  config.pins{end+1} = raspberryio.config.pin_info(10, {"digital", "pwm", "spi_mosi"});
  config.pins{end+1} = raspberryio.config.pin_info(11, {"digital", "pwm", "spi_sclk"});
  config.pins{end+1} = raspberryio.config.pin_info(12, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(13, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(14, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(15, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(16, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(17, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(18, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(19, {"digital", "pwm", "spi1_miso"});
  config.pins{end+1} = raspberryio.config.pin_info(20, {"digital", "pwm", "spi1_mosi"});
  config.pins{end+1} = raspberryio.config.pin_info(21, {"digital", "pwm", "spi1_sclk"});
  config.pins{end+1} = raspberryio.config.pin_info(22, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(23, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(24, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(25, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(26, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(27, {"digital", "pwm"});

  typeval = bitand(bitshift(id, -4), int32(0xff));

  # zero
  if typeval == 9 || typeval == 12 || typeval == 18
    config.pins{end+1} = raspberryio.config.pin_info(47, {"led0"});
  else
    config.pins{end+1} = raspberryio.config.pin_info(35, {"led1"});
    config.pins{end+1} = raspberryio.config.pin_info(47, {"led0"});
  endif

  config.pinout = {
    "    GPIO   pin pin   GPIO",	
    " 3V3 -      1   2     -  5V",
    " SDA -      3   4     -  5V",
    " SCL -      5   6     -  GNN",
    "     4      7   8     14 TXD",
    " GND -      9   10    15 RXD",
    " ce1 17     11  12    18 ce0",
    "     27     13  14    -  GND",
    "     22     15  16    23	",
    " 3V3 -      17  18    24",
    "MOSI 10     19  20    -  GND",
    "MISO 9      21  22    25",	
    "SCLK 11     23  24    8  CE0",
    " GND -      25  26    7  CE1",
    "IDSD 0      27  28    1  IDSC",
    "     5      29  30    -  GND",
    "     6      31  32    12",
    "     13     33  34    -  GND",
    "miso 19     35  36    16 ce2",
    "     26     37  38    20 mosi",
    "GND  -      39  40    21 sclk"
  };
 
endfunction
