function config = config_2 (id)
  config = [];
  config.pins = {};
  # User GPIO 2-4, 7-11, 14-15, 17-18, 22-25, 27-31
  config.pins{end+1} = raspberryio.config.pin_info(2, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(3, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(4, {"digital", "pwm"});

  config.pins{end+1} = raspberryio.config.pin_info(7, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(8, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(9, {"digital", "pwm", "spi_miso"});
  config.pins{end+1} = raspberryio.config.pin_info(10, {"digital", "pwm", "spi_mosi"});
  config.pins{end+1} = raspberryio.config.pin_info(11, {"digital", "pwm", "spi_clk"});

  config.pins{end+1} = raspberryio.config.pin_info(14, {"digital", "pwm", "uart_tx"});
  config.pins{end+1} = raspberryio.config.pin_info(15, {"digital", "pwm", "uart_rx"});

  config.pins{end+1} = raspberryio.config.pin_info(16, {"led0"});

  config.pins{end+1} = raspberryio.config.pin_info(17, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(18, {"digital", "pwm"});

  config.pins{end+1} = raspberryio.config.pin_info(21, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(22, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(23, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(24, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(25, {"digital", "pwm"});

  config.pins{end+1} = raspberryio.config.pin_info(27, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(28, {"digital", "pwm", "i2c1_sda"});
  config.pins{end+1} = raspberryio.config.pin_info(29, {"digital", "pwm", "i2c1_scl"});
  config.pins{end+1} = raspberryio.config.pin_info(30, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(31, {"digital", "pwm"});

  config.pinout = {
    "    GPIO   pin pin   GPIO",	
    " 3V3 -      1   2     -  5V",
    " SDA 0      3   4     -  5V",
    " SCL 1      5   6     -  GND",
    "     4      7   8     14 TXD",
    " GND -      9   10    15 RXD",
    "ce1  17     11  12    18 ce0",
    "     21     13  14    -  GND",
    "     22     15  16    23",
    " 3V3 -      17  18    24",
    "MOSI 10     19  20    -  GND",
    "MISO 9      21  22    25",	
    "SCLK 11     23  24    8  CE0",
    " GND -      25  26    7  CE1",
    "",
    "    GPIO   pin pin   GPIO",	
    " 5V  -      1   2     -  3V3",
    "SDA  28     3   4     29 SCL",
    "     30     5   6     31",
    " GND -      7   8     -  GND"
  };
 
endfunction
