function config = config_1 (id)
  config = [];
  config.pins = {};
  # User GPIO 0-1, 4, 7-11, 14-15, 17-18, 21-25
  config.pins{end+1} = raspberryio.config.pin_info(0, {"digital", "pwm", "i2c0_sda"});
  config.pins{end+1} = raspberryio.config.pin_info(1, {"digital", "pwm", "i2c0_scl"});
  config.pins{end+1} = raspberryio.config.pin_info(4, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(7, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(8, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(9, {"digital", "pwm", "spi_miso"});
  config.pins{end+1} = raspberryio.config.pin_info(10, {"digital", "pwm", "spi_mosi"});
  config.pins{end+1} = raspberryio.config.pin_info(11, {"digital", "pwm", "spi_clk"});

  config.pins{end+1} = raspberryio.config.pin_info(14, {"digital", "pwm", "uart_tx"});
  config.pins{end+1} = raspberryio.config.pin_info(15, {"digital", "pwm", "uart_rx"});

  config.pins{end+1} = raspberryio.config.pin_info(16, {"led0"});

  config.pins{end+1} = raspberryio.config.pin_info(17, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(18, {"digital", "pwm"});

  config.pins{end+1} = raspberryio.config.pin_info(21, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(22, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(23, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(24, {"digital", "pwm"});
  config.pins{end+1} = raspberryio.config.pin_info(25, {"digital", "pwm"});

  config.pinout = {
    "    GPIO   pin pin   GPIO",	
    " 3V3 -      1   2     -  5V",
    " SDA 0      3   4     -  5V",
    " SCL 1      5   6     -  GND",
    "     4      7   8     14 TXD",
    " GND -      9   10    15 RXD",
    "ce1  17     11  12    18 ce0",
    "     21     13  14    -  GND",
    "     22     15  16    23",
    " 3V3 -      17  18    24",
    "MOSI 10     19  20    -  GND",
    "MISO 9      21  22    25",	
    "SCLK 11     23  24    8  CE0",
    " GND -      25  26    7  CE1"
  };
 
endfunction
