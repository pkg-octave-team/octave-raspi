## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.


# example blink of LED

function example_dio_blink_led ()
  p = raspi("raspberrypi.local")

  while 1
    writeDigitalPin(p, 17, 0);
    pause(.5);
    writeDigitalPin(p, 17, 1);
    pause(.5);

  endwhile

endfunction
