## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

# example using serial port to read data from gps

function example_serial_gps
  p = raspi("raspberrypi.local")

  s = serialdev(p, '/dev/serial0', 9600);

  # send firmware release query command
  write(s, "$PMTK605*31\r\n");

  while 1
    # just receive and display the raw data
    data = read(s, 500);
    if !isempty(data)
      printf("%s", char(data));
    endif
    pause(.5);
  endwhile

endfunction
