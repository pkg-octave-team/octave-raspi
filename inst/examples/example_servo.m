## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

# example using servo on pin gpio 17

function example_servo
  p = raspi("raspberrypi.local")

  s = servo(p, 17, 'MinPulseDuration', 1e-3, 'MaxPulseDuration', 2e-3)

  # set pos to mid position
  pos = 90;
  speed = 3;
  pauseval = .1;

  writePosition (s, 90);

  printf ("scanning ...\n");
  while (true)
    pos = pos + speed;
    if(pos > 180)
      pos = 180;
      speed = -speed;
    endif
    if(pos < 0)
      pos = 0;
      speed = -speed;
    endif

    writePosition (s, pos); 
    pause (pauseval);

  endwhile

  clear s
endfunction
