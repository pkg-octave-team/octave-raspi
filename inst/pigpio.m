## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

classdef pigpio < pigpio_base
  ## -*- texinfo -*- 
  ## @deftypefn {} {@var{retval} =} pigpio ()
  ## @deftypefnx {} {@var{retval} =} pigpio (@var{ipaddress}, @var{portnumber})
  ## @deftypefnx {} {@var{retval} =} pigpio (@var{ipaddress}, @var{portnumber}, @var{timeout})
  ##
  ## Connect to pigpio server on a raspberry pi board using the pigpio daemon interface.
  ##
  ## @subsubheading Inputs
  ## @var{ipaddress} - ip or host name of the raspberry PI pigpio server. If
  ## omitted, a default of "127.0.0.1" is used.
  ##
  ## @var{portnumber} - port number the server is running on. If omitted, a port of 8888 is used.
  ##
  ## @var{timeout} - Timeout value.
  ##
  ## @subsubheading Outputs
  ## @var{retval} - a pigpio object.
  ##
  ## @seealso{http://abyz.me.uk/rpi/pigpio/pdif2.html}
  ## @end deftypefn
  ##
  ## @deftypefn {} {@var{retval} =} get_hardware_revision (@var{p})
  ##
  ## Get the raspberry pi hardware version
  ##
  ## @subsubheading Inputs
  ## @var{p} - the connected pigpio object.
  ##
  ## @subsubheading Outputs
  ## @var{retval} - hardware version number.
  ##
  ## @seealso{pigpio}
  ## @end deftypefn
  ##
  ## @deftypefn {} {@var{retval} =} get_pigpio_version (@var{p})
  ##
  ## Get the pigpio software version
  ##
  ## @subsubheading Inputs
  ## @var{p} - the connected pigpio object.
  ##
  ## @subsubheading Outputs
  ## @var{retval} - version number.
  ##
  ## @seealso{pigpio}
  ## @end deftypefn
  ##
  ## @deftypefn {} {@var{retval} =} get_current_tick (@var{p})
  ##
  ## Get the a tick value in milliseconds
  ##
  ## @subsubheading Inputs
  ## @var{p} - the connected pigpio object.
  ##
  ## @subsubheading Outputs
  ## @var{retval} - tick value.
  ##
  ## Note that as the tick value is a int32,
  ## the value will wrap every 17 minutes.
  ##
  ## @seealso{pigpio}
  ## @end deftypefn
  ##
  ## @deftypefn {} {@var{retval} =} get_mode (@var{p}, @var{pin})
  ##
  ## Get the mode a pin is set to.
  ##
  ## @subsubheading Inputs
  ## @var{p} - the connected pigpio object.@*
  ## @var{pin} - the pin to query.
  ##
  ## @subsubheading Outputs
  ## @var{retval} - pin mode.
  ##
  ## @seealso{pigpio}
  ## @end deftypefn
  ##
  ## @deftypefn {} {@var{retval} =} set_mode (@var{p}, @var{pin}, @var{mode})
  ##
  ## Set the mode a pin is set to.
  ##
  ## @subsubheading Inputs
  ## @var{p} - the connected pigpio object.@*
  ## @var{pin} - the pin to query.@*
  ## @var{mode} - mode to.@*
  ##
  ## @subsubheading Outputs
  ## @var{retval} - 0 on success.
  ##
  ## @seealso{pigpio}
  ## @end deftypefn

  properties (Access = public)
    debug = false;
    timeout = 10;
  endproperties
  properties (SetAccess = private)
    connectinfo = [];
  endproperties
  properties (Access = private)
    socket = [];
  endproperties
  properties (Access = public, Constant = true)
    # pin modes
    PI_MODE_INPUT  = 0;
    PI_MODE_OUTPUT = 1;
    PI_MODE_ALT5 = 2;
    PI_MODE_ALT4 = 3;
    PI_MODE_ALT0 = 4;
    PI_MODE_ALT1 = 5;
    PI_MODE_ALT2 = 6;
    PI_MODE_ALT3 = 7;

    # pull up mode
    PI_PUD_OFF = 0;
    PI_PUD_DOWN = 1;
    PI_PUD_UP = 2;
  endproperties

  methods (Access = public)
    function this = pigpio (varargin)

      if nargin >= 1
        host = varargin{1};
      else
        host = "127.0.0.1";
      endif

      if nargin >= 2
        port = varargin{2};
      else
        port = 8888;
      endif

      if nargin >= 3
        this.timeout = varargin{3};
      endif

      ip = resolvehost (host, 'address');
      this.socket = tcp (ip, port);
      # set 0 timeout on socket - we will handle ourselves
      set (this.socket, 'timeout', 0);
 
    endfunction

    function disp (this)
       printf("  %s object with properties:\n", class(this));
       printf("      ipaddress = %s\n", get(this.socket, "remotehost"));
       printf("           port = %d\n", get(this.socket, "remoteport"));
       printf("         status = %s\n", get(this.socket, "status"));
       printf("        timeout = %f\n", this.timeout);
       printf("          debug = %d\n", this.debug);
    endfunction

    function ver = get_hardware_revision (this)
      data = sendMessage (this, this.PI_CMD_HWVER, 0, 0, 0);
      ver = data (4);
    endfunction

    function ver = get_pigpio_version (this)
      data = sendMessage (this, this.PI_CMD_PIGPV, 0, 0, 0);
      ver = data (4);
    endfunction

    function value = get_current_tick (this)
      data = sendMessage (this, this.PI_CMD_TICK, 0, 0, 0);
      value = data (4);
    endfunction

    function mode = get_mode (this, pin)
      if nargin < 2 || !isnumeric(pin)
        error ("Expected numeric pin");
      endif
      data = sendMessage (this, this.PI_CMD_MODEG, pin, 0, 0);
      mode = data (4);
    endfunction

    function res = set_mode (this, pin, mode)
      if nargin < 3 || !isnumeric(pin) || !isnumeric(mode)
        error ("Expected numeric pin and mode");
      endif
 
      data = sendMessage (this, this.PI_CMD_MODES, pin, mode, 0);
      res = data (4);
    endfunction

    function res = gpio_read (this, pin)
      if nargin < 2 || !isnumeric(pin)
        error ("Expected numeric pin");
      endif
      data = sendMessage (this, this.PI_CMD_READ, pin, 0, 0);
      res = data (4);
    endfunction

    function res = gpio_write (this, pin, value)
      if nargin < 3 || !isnumeric(pin) || !isnumeric(value)
        error ("Expected numeric pin and value");
      endif
      data = sendMessage (this, this.PI_CMD_WRITE, pin, value, 0);
      res = data (4);
    endfunction

    function res = set_PWM_frequency (this, pin, value)
      if nargin < 3 || !isnumeric(pin) || !isnumeric(value)
        error ("Expected numeric pin and value");
      endif
      data = sendMessage (this, this.PI_CMD_PFS, pin, value, 0);
      res = data (4);
    endfunction

    function res = get_PWM_frequency (this, pin)
      if nargin < 2 || !isnumeric(pin)
        error ("Expected numeric pin");
      endif
      data = sendMessage (this, this.PI_CMD_PFG, pin, 0, 0);
      res = data (4);
    endfunction

    function res = set_PWM_dutycycle (this, pin, value)
      if nargin < 3 || !isnumeric(pin) || !isnumeric(value)
        error ("Expected numeric pin and value");
      endif
      data = sendMessage (this, this.PI_CMD_PWM, pin, value, 0);
      res = data (4);
    endfunction

    function res = get_PWM_dutycycle (this, pin)
      if nargin < 2 || !isnumeric(pin)
        error ("Expected numeric pin");
      endif
      data = sendMessage (this, this.PI_CMD_GDC, pin, 0, 0);
      res = data (4);
    endfunction

    function res = set_servo_pulsewidth (this, pin, value)
      if nargin < 3 || !isnumeric(pin) || !isnumeric(value)
        error ("Expected numeric pin and value");
      endif
      data = sendMessage (this, this.PI_CMD_SERVO, pin, value, 0);
      res = data (4);
    endfunction

    function res = get_servo_pulsewidth (this, pin)
      if nargin < 3 || !isnumeric(pin)
        error ("Expected numeric pin");
      endif
      data = sendMessage (this, this.PI_CMD_GPW, pin, 0, 0);
      res = data (4);
    endfunction

    function res = i2c_open (this, bus, addr, flags)
      if nargin < 3 || !isnumeric(bus) || !isnumeric(addr)
        error ("Expected numeric bus and address");
      endif
 
      if nargin < 4
        flags = 0;
      endif
      data = sendMessage (this, this.PI_CMD_I2CO, bus, addr, 4, typecast(uint32([0]), 'uint8'));
      res = data (4);
    endfunction

    function res = i2c_close (this, i2c)
      if nargin < 2 || !isnumeric(i2c)
        error ("Expected numeric i2c handle");
      endif
      data = sendMessage (this, this.PI_CMD_I2CC, i2c, 0, 0);
      res = data (4);
    endfunction

    function res = i2c_write_device (this, i2c, data)
      if nargin < 3 || !isnumeric(i2c)
        error ("Expected numeric i2c handle and data to write");
      endif
      data = [uint8(data)];
      len = length(data);
      data = sendMessage (this, this.PI_CMD_I2CWD, i2c, 0, len, data);
      res = data (4);
    endfunction

    function res = i2c_read_device (this, i2c, cnt)
      if nargin < 3 || !isnumeric(i2c) || !isnumeric(cnt)
        error ("Expected numeric i2c handle and count");
      endif
      [data, ext] = sendMessage (this, this.PI_CMD_I2CRD, i2c, cnt, 0);
      len = data (4);
      if len >= 0
        res = ext;
      else
        error ("i2c_read_device failed to read - %s", this.error_text(len));
      endif
 
    endfunction

    function res = i2c_read_i2c_block_data (this, i2c, reg, cnt)
      if nargin < 4 || !isnumeric(i2c) || !isnumeric(cnt) || !isnumeric(reg)
        error ("Expected numeric i2c handle, reg and count");
      endif
      [data, ext] = sendMessage (this, this.PI_CMD_I2CRI, i2c, reg, 4, typecast(uint32([cnt]), 'uint8'));
      len = data (4);
      if len >= 0
        res = ext;
      else
        error ("i2c_read_i2c_block_data failed to read - %s", this.error_text(len));
      endif
    endfunction

    function res = i2c_write_i2c_block_data (this, i2c, reg, data)
      if nargin < 4 || !isnumeric(i2c) || !isnumeric(reg)
        error ("Expected numeric i2c handle, reg and data");
      endif
      data = [uint8(data)];
      len = length(data);
      [data, ext] = sendMessage (this, this.PI_CMD_I2CWI, i2c, reg, len, data);
      res = data (4);
    endfunction

    function res = i2c_read_block_data (this, i2c, reg)
      if nargin < 3 || !isnumeric(i2c) || !isnumeric(reg)
        error ("Expected numeric i2c handle and reg");
      endif
      [data, ext] = sendMessage (this, this.PI_CMD_I2CRK, i2c, reg);
      len = data (4);
      if len >= 0
        res = ext;
      else
        error ("i2c_read_block_data failed to read - %s", this.error_text(len));
      endif
    endfunction

    function res = i2c_write_block_data (this, i2c, reg, data)
      if nargin < 4 || !isnumeric(i2c) || !isnumeric(reg)
        error ("Expected numeric i2c handle, reg and data");
      endif
      data = [uint8(data)];
      len = length(data);
      [data, ext] = sendMessage (this, this.PI_CMD_I2CWK, i2c, reg, len, data);
      res = data (4);
    endfunction

    function res = i2c_quick_write (this, i2c, bit)
      if nargin < 3 || !isnumeric(i2c) || !isnumeric(bit)
        error ("Expected numeric i2c handle, reg and bit");
      endif
      if bit < 0 || bit > 1
        error ('Expected bit value of 0 or 1.');
      endif
      # bit is 0 or 1
      [data, ext] = sendMessage (this, this.PI_CMD_I2CWQ, i2c, bit);
      res = data (4);
    endfunction

    function res = spi_open (this, channel, baud, flags)
      if nargin < 3 || !isnumeric(channel) || !isnumeric(baud)
        error ("Expected numeric channel and baud");
      endif
      if nargin < 4
        flags = 0;
      endif
      data = sendMessage (this, this.PI_CMD_SPIO, channel, baud, 4, typecast(uint32(flags), 'uint8'));
      res = data (4);
    endfunction

    function res = spi_close (this, spi)
      if nargin < 2 || !isnumeric(spi)
        error ("Expected numeric spi handle");
      endif
      data = sendMessage (this, this.PI_CMD_SPIC, spi, 0, 0);
      res = data (4);
    endfunction

    function res = spi_read (this, spi, count)
      if nargin < 3 || !isnumeric(spi) || !isnumeric(count)
        error ("Expected numeric spi handle and count");
      endif
      [data, ext] = sendMessage (this, this.PI_CMD_SPIR, spi, count, 0);
      len = data (4);
      if len >= 0
        res = ext;
      else
        error ("spi_read failed - %s", this.error_text(len));
      endif
    endfunction

    function res = spi_write (this, spi, data)
      if nargin < 3 || !isnumeric(spi)
        error ("Expected numeric spi handle and data");
      endif
      data = [uint8(data)];
      len = length(data);
      [data, ext] = sendMessage (this, this.PI_CMD_SPIW, spi, 0, len, data);
      len = data (4);
      if len >= 0
        res = ext;
      else
        error ("spi_write failed - %s", this.error_text(len));
      endif
    endfunction

    function res = spi_xfer (this, spi, data)
      if nargin < 3 || !isnumeric(spi)
        error ("Expected numeric spi handle and data");
      endif
      data = [uint8(data)];
      len = length(data);
      [data, ext] = sendMessage (this, this.PI_CMD_SPIX, spi, 0, len, data);
      len = data (4);
      if len >= 0
        res = ext;
      else
        error ("spi_xfer failed to writeRead - %s", this.error_text(len));
      endif
    endfunction

    function res = shell (this, scriptname, scriptstr)
      if nargin < 2 || !ischar(scriptname)
        error ("Expected script name");
      endif
      if nargin < 3
        scriptstr = "";
      endif
      clen = length(uint8(scriptname));
      data = [uint8(scriptname) uint8(0) uint8(scriptstr)];
      [data, ext] = sendMessage (this, this.PI_CMD_SHELL, clen, 0, length(data), data);
      len = data (4);
      if (len == 32512)
        error ("shell script not found");
      elseif len/256 < 0 
        error ("shell failed - %s", this.error_text(len/256));
      endif
      res = len/256;
    endfunction

    function res = serial_open (this, port, baud, flags)

      if nargin < 4
        flags = 0;
      endif

      len = length(port);
      data = sendMessage (this, this.PI_CMD_SERO, baud, flags, len, typecast(port, 'uint8'));
      res = data (4);
    endfunction

    function res = serial_close (this, ser)
      data = sendMessage (this, this.PI_CMD_SERC, ser, 0, 0);
      res = data (4);
    endfunction

    function res = serial_write (this, ser, data)
      data = [uint8(data)];
      len = length(data);
      [data, ext] = sendMessage (this, this.PI_CMD_SERW, ser, 0, len, data);
      res = data (4);
    endfunction

    function res = serial_read (this, ser, count)
      [data, ext] = sendMessage (this, this.PI_CMD_SERR, ser, count, 0);
      len = data (4);
      if len >= 0
        res = ext;
      else
        error ("serialdev failed to read - %s", this.error_text(len));
      endif
    endfunction

    function res = serial_data_available (this, ser)
      [data, ext] = sendMessage (this, this.PI_CMD_SERDA, ser, 0);
      res = data (4);
    endfunction

    # file must be in dir allowed from /opt/pigpio/access
    function res = file_open (this, file, mode)
      if nargin < 3
        error ("Expected filename and mode");
      endif

      len = length(file);
      data = sendMessage (this, this.PI_CMD_FO, mode, 0, len, file);
      res = data (4);
    endfunction

    function res = file_close (this, fd)
      data = sendMessage (this, this.PI_CMD_FC, fd, 0, 0);
      res = data (4);
    endfunction

    function res = file_write (this, fd, data)
      data = uint8(data);
      if (size(data, 1) > 1)
        data = data';
      endif
      len = length(data);
      [data, ext] = sendMessage (this, this.PI_CMD_FW, fd, 0, len, data);
      res = data (4);
    endfunction

    function res = file_read (this, fd, count)
      [data, ext] = sendMessage (this, this.PI_CMD_FR, fd, count, 0);
      len = data (4);
      if len >= 0
        res = ext;
      else
        error ("file_read failed to read - %s", this.error_text(len));
      endif
    endfunction

    function res = file_seek (this, fd, off, where)
      [data, ext] = sendMessage (this, this.PI_CMD_FS, fd, off, 4, typecast(uint32(where), 'uint8'));
      res = data (4);
    endfunction

    function res = file_list (this, pattern)
      len = length(pattern);
      [data, ext] = sendMessage (this, this.PI_CMD_FL, 60000, 0, len, pattern);
      len = data (4);
      if len >= 0
        res = char(ext);
      else
	# no pattern match
        if len == -136
          res = "";
	else
          error ("fileList failed to read - %s", this.error_text(len));
	endif
      endif
 
    endfunction

  endmethods

  methods (Access = private)
    function v = get.connectinfo (this)
        v = get(this.socket)
    endfunction

    function [data, ext] = sendMessage (this, cmd, p1, p2, p3, ext)
       
      persistent endian;
      if isempty(endian)
        [~, ~, endian] = computer ();
      endif

      if nargin < 5
        p3 = 0;
      endif
      
      if nargin < 6 
        ext = [];
      endif

      # clear any pending data
      avail = get(this.socket, 'bytesavailable');
      while (avail > 0)
        tmp = fread(this.socket, avail);
        avail = get(this.socket, 'bytesavailable');
      endwhile
      
      outdata = [int32(cmd) int32(p1) int32(p2) int32(p3)];
      
      if endian == 'B'
        outdata = swapbytes (outdata);
      endif
      
      outdata = typecast (outdata, 'uint8');

      if !isempty(ext)
        ext = typecast (ext, 'uint8');	   
        outdata = [outdata ext];
      endif
      
      if(this.debug)
        printf ("> ")
        printf ("%02x ", outdata)
        printf ("\n")
        #printf ("> %s\n", char(data))
      endif

      fwrite(this.socket, outdata);

      avail = 0;
      timeout = this.timeout;
      avail = get(this.socket, 'bytesavailable');
      while avail == 0 && timeout > 0
        pause(.1);
        avail = get(this.socket, 'bytesavailable');
        timeout -= .1;
      endwhile

      if avail > 0 && avail < 16
        error ("Expected at least 16 bytes of data");
      endif

      if avail > 0
        indata = fread(this.socket, avail);
        if(this.debug)
          printf ("< ")
          printf ("%02x ", indata)
          printf ("\n")
        endif

        # parse the http data
        data = typecast (indata(1:16), 'int32');
        ext = indata(17:end);

        if endian == 'B'
          data = swapbytes (data);
        endif
        
      else
          error ("sendMessage timed out");
      endif
      
    endfunction
  endmethods
endclassdef

%!test
%! p = pigpio ("raspberrypi.local", 8888);
%! assert(isa(p, "pigpio"));
%! assert(get_hardware_revision(p) > 0);
%! assert(get_pigpio_version(p) > 0);
%! clear p
