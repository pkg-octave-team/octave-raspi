## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} deleteFile (@var{obj}, @var{filename})
## Delete a file from the connected pi.
##
## @subsubheading Inputs
## @var{obj} - connected raspi object.
##
## @var{filename} - file name to delete, or wildcard string.
##
## @seealso{raspi, getFile, putFile}
##
## @end deftypefn

function deleteFile (obj, filename)

  if nargin != 2
    error ("Expected filename");
  endif

  if ! ischar (filename)
    error ("filename should be a string");
  endif

  # get list of files that match the input
  files = file_list (obj.connected, filename);

  if !isempty(files)
    files = strsplit(files, {"\n", "\r"}, 'COLLAPSEDELIMITERS', true);

    # for each file, attempt to delete it
    for i=1:length (files)
      if !isempty (files{i})
        res = shell (obj.connected, "raspi_deletefile", files{i});
	if res != 0
          warning ("Did not delete '%s' - %d", files{i}, res);
	endif
      endif
    endfor
  else
    warning ("no file '%s' found to delete.", filename);
  endif

endfunction

%!test
%! # tested in putFile
