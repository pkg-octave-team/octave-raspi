## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} scanI2Cbus (@var{piobj})
## @deftypefnx {} {@var{retval} =} scanI2Cbus (@var{piobj}, @var{bus})
## Scan pi for devices on the I2C bus.
##
## @subsubheading Inputs
## @var{piobj} - raspi object connected to a Raspberry Pi board.
##
## @var{bus} - bus number to scan I2C devices, when multiple buses are available.
## If the bus is not specified, it will default to 0.
## 
## @subsubheading Outputs
## @var{retval} - cell array of addresses as strings in format of "0xXX".
##
## @subsubheading Example
## @example
## @code {
## # create pi connection.
## p = raspi('raspberrypi.local');
## # scan for devices on the I2C bus
## scanI2Cbus (p, 1)
## # output is each detected I2C address as a string
## ans =
## @{
##  [1,1] = 0x50
## @}
## }
## @end example
##
## @seealso{raspi, i2cdev}
## @end deftypefn

function addr = scanI2Cbus (p, bus)

  addr = {};

  if nargin < 1 || nargin > 2
    print_usage ();
  endif

  if nargin == 1
    bus = 0;
  elseif ischar (bus)
    # bus will be i2c-1 or i2c-1 etc
    bus = sscanf (bus, "i2c-%d");
    if isempty(bus)
      error("scanI2Cbus: expected bus i2c-X");
    endif
  endif

  if !isnumeric (bus) || bus < 0 || bus > 1
    error ('scanI2Cbus: expected bus to be numeric and 0 or 1');
  endif

  if (!isa (p, "raspi"))
    error ("scanI2Cbus: expects raspi object as first argument");
  endif
  
  # scan each address, and add any found to cell array
  for i = 3:0x77
     if checkI2CAddress (p, i, bus)
       addr{end+1} = [ "0x" dec2hex(i, 2) ];
     endif
  endfor

endfunction

# private func to scan a single address
function yes = checkI2CAddress (p, addr, bus)
  yes = false;
  try
    i2c = i2c_open (p.connected, bus, addr);
    if i2c < 0
      error ("i2c address %02X Not opened", addr)
    endif
    unwind_protect
      res = i2c_quick_write (p.connected, i2c, 0);
      if res == 0
        yes = true;
      endif
    unwind_protect_cleanup
      i2c_close (p.connected, i2c);
    end_unwind_protect
  catch
    # do nothing
    printf ("scanI2Cbus: %s\n", lasterror.message);
  end_try_catch
endfunction

%!test
%! p = raspi ('raspberrypi.local');
%! assert(!isempty(p));
%! scanI2Cbus (p, p.AvailableI2CBuses{1});
