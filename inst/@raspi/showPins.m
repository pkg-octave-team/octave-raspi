## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} showPins (@var{obj})
## Show the locations of pins on the raspi.
##
## @subsubheading Inputs
## @var{obj} - connected raspi object.
##
## @seealso{raspi}
## @end deftypefn

function out = showPins (obj)
  # just dump out the pinout
  pins = obj.config.pinout;
  show = "";
  for i =1:length (pins)
    show = [show sprintf("%s\n", pins{i})];
  endfor
  if nargout > 0
    out = show;
  else
    printf("%s\n", show);
  endif
endfunction

%!test
%! pi = raspi("raspberrypi.local");
%! a = showPins(pi);

%!error <undefined> showPins()
