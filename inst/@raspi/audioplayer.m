## Copyright (C) 2020-2021 John D <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

classdef audioplayer
  ## -*- texinfo -*- 
  ## @deftypefn {} {@var{retval} =} audioplayer (@var{piobj}, @var{devname})
  ## @deftypefnx {} {@var{retval} =} audioplayer (@var{piobj}, @var{devname}, @var{propertyname}, @var{propertyvalue})
  ##
  ## Create a audio output object to a raspberry pi
  ##
  ## @subheading Inputs
  ## @var{piobj} - a connected raspberry pi raspi object.@*
  ## @var{devname} - name of the audio livice to output to.@*
  ## @var{propertyname}, @var{propertyvalue} - Optional property name/value pairs.@*
  ##
  ## @subheading Outputs
  ## @var{retval} - returns a audioplayer object.
  ##
  ## @subheading Object Properties
  ## Known properties are:
  ## @table @asis
  ## @item DeviceName
  ## The name of device to us.
  ## @item SampleRate
  ## The samplerate to use
  ## @item Parent
  ## Raspi object of the audioplayer.
  ## @end table
  ##
  ## @seealso{raspi, listAudioDevices}
  ## @end deftypefn
  ##
  ## @deftypefn {} {} play (@var{audobj}, @var{data})
  ## Play the input data on the audioplayer
  ##
  ## @subheading Inputs
  ## @var{audobj} - the audioplayer object.@*
  ## @var{data} - the data to write to the device (int16).
  ##
  ## @subheading Outputs
  ## None
  ##
  ## @seealso{spidev}
  ## @end deftypefn

  properties (GetAccess = public, SetAccess = private)
    Parent = [];
    DeviceName = [];
    SampleRate = 44100;
  endproperties

  methods
    function this = audioplayer (p, devname, varargin)
      this.Parent = p;

      if nargin < 2
        error ("expected raspi and device name");
      endif

      if !ischar(devname)
        error ("Expected device name to be a string");
      endif

      this.DeviceName = devname;
      # hw:0,0

      # TODO: check we have that device

      # TODO: set properties if any
    endfunction

    function play(this, data)
      if nargin < 2 || !isa(data, 'int16')
        error ("Expected int16 data")
      endif

      nok = __playaudio__(this.Parent, this.DeviceName, this.SampleRate, data);
      
    endfunction

    function disp (this)
      printf("  %s object with properties of:\n", class(this));
      printf("       DeviceName = %s\n", this.DeviceName);
      printf("       SampleRate = %d\n", this.SampleRate);
    endfunction
  endmethods

endclassdef

%!test
%! pi = raspi("raspberrypi.local");
%! # TODO
