## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} openShell (@var{obj})
## Open a new terminal window initiating a ssh session
##
## @subsubheading Inputs
## @var{obj} - connected raspi object.
##
## @seealso{raspi, system}
##
## @end deftypefn

function openShell (obj)

  addr = obj.DeviceAddress;

  prog = file_in_loadpath("bin/raspi_term.sh");

  if isempty(prog)
    error ("Couldnt find raspi_term.sh");
  endif

  ssh = file_in_path(getenv('PATH'), {'ssh', 'ssh.exe'});
  if isempty(ssh)
    error ("Couldnt find ssh program in path");
  endif

  # find a terminal to use
  if ispc()
    term = "cmd /c start sh -c";
    prog = strrep(prog, '\', '/');
  else
    # TODO be a little more selective of what shell to try
    # based on desktop env that is running
    term = file_in_path(getenv('PATH'), {'konsole', 'mate-terminal', 'gnome-terminal', 'xterm'});

    if isempty(term)
      error ("Couldnt find a terminal application to use.");
    endif

    term = sprintf("%s -e", term);
  endif

  # run the command
  cmd = sprintf("%s '%s  %s' &", term, prog, addr);
  if obj.Debug
    cmd
    system(cmd)
  else
    system(cmd);
  endif

endfunction

