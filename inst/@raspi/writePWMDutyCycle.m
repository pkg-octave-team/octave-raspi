## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} writePWMDutyCyle (@var{ar}, @var{pin}, @var{value})
## Set pin to output a square wave with a specified duty cycle.
##
## @subsubheading Inputs
## @var{ar} - connected raspi object
##
## @var{pin} - pin to write to.
##
## @var{value} - duty cycle value where 0 = off, 0.5 = 50% on, 1 = always on.
##
## @subsubheading Example
## @example
## @code{
## p = raspi();
## writePWMDutyCycle(p, 17, 0.5);
## }
## @end example
##
## @seealso{raspi, writePWMVoltage}
##
## @end deftypefn

function writePWMDutyCycle (obj, pin, value)
  if nargin < 3
    error ("@raspi.writePWMDutyCycle: expected pin name and value");
  endif
  if !isnumeric (pin)
    error ("@raspi.writePWMDutyCycle: expected numeric pin");
  endif
  if (!isnumeric (value) || value > 1.0 || value < 0)
    error ("@raspi.writePWMDutyCycle: expected value between 0 .. 1");
  endif  

  pininfo = obj.get_pin (pin);
  if !strcmp("pwm", pininfo.mode)
    configurePin(obj, pin, "pwm");
  endif
 
  set_PWM_dutycycle(obj.connected, pin, 256.0*value);
  
endfunction

%!shared ar
%! ar = raspi("raspberrypi.local");

%!test
%! writePWMDutyCycle(ar, 4, 0.5);

%!error <undefined> writePWMDutyCycle();

%!error <expected> writePWMDutyCycle(ar)

%!error <expected pin> writePWMDutyCycle(ar, 4)

%!error <unknown pin> writePWMDutyCycle(ar, -1, 1)

%!error <expected value between> writePWMDutyCycle(ar, 4, -1)

%!error <expected value between> writePWMDutyCycle(ar, 4, 1.1)

%!test
%! writePWMDutyCycle(ar, 4, 0.0);
