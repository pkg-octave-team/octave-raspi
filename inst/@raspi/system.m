## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {@var{output} =} system (@var{obj}, @var{cmd})
## @deftypefnx {} {@var{output} =} system (@var{obj}, @var{cmd}, @var{sudo})
## Control an on board led.
##
## @subsubheading Inputs
## @var{obj} - connected raspi object.
##
## @var{cmd} - command string to execute
##
## @var{sudo} - string 'sudo' to run command as sudo.
##
## @subsubheading Outputs
## @var{output} - output from running the command
##
## @seealso{raspi}
##
## @end deftypefn

function output = system (obj, cmd, sudo)

  if nargin < 2
    error ("Expected command");
  endif

  if ! ischar (cmd)
    error ("cmd should be a string");
  endif

  if nargin < 3
    sudo = "";
  elseif ! ischar (sudo) || !strcmp (sudo,"sudo")
    error ("expected 'sudo' as third argument");
  endif

  id = getpid ();

  if !isempty (sudo)
    cmd = sprintf ("%d %s -c %s", id, sudo, cmd);
  else
    cmd = sprintf ("%d %s", id, cmd);
  endif

  # run the command
  res = shell (obj.connected, "raspi_system", cmd);

  # attempt open and read the output of the command
  output = "";
  filename = sprintf ("/tmp/raspi-sys-%d", id);
  fd = file_open (obj.connected, filename, obj.connected.FILE_READ);
  if (fd >= 0)

    while true
      rd = file_read (obj.connected, fd, 1024);
      if !isempty (rd)
        output = [char(rd) output];
      else
        break;
      endif
    endwhile

    file_close (obj.connected, fd);
  endif

endfunction

%!shared pi
%! pi = raspi("raspberrypi.local");

%!test
%! a = system (pi, 'ls /');
%! assert(!isempty(a));

%!error <Expected command> system(pi)
%!error <cmd should be a string> system(pi, 4)
%!error <expected 'sudo' as third argument> system(pi, 'ls', 'notadmin')

