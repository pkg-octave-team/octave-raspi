## Copyright (C) 2020-2021 John D <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} listAudioDevices (@var{p}, @var{audioType})
##
## Query the available audio devices on the Raspberry Pi.
##
## @subsubheading Inputs
## @var{p} - Raspi to query.
##
## @var{audioType} - type of audio device to query. Valid options 'playback' or 'capture'
##
## @subsubheading Outputs
## @var{devices} - a List of structs for the available devices.
## with the struct containing Name, Device, Channels and SamplingRate.
##
## @seealso{raspi, audioplayer}
## @end deftypefn
 
function  devices = listAudioDevices (p, type)

  if nargin != 2
    error ("Expected raspi object and audioType");
  endif

  devices = {};

  if strcmp(type, "capture")
    devicedata = system(p, "cat /proc/asound/card*/pcm*c/info");
  elseif strcmp(type, "playback")
    devicedata = system(p, "cat /proc/asound/card*/pcm*p/info");
  else
    error ("Expected audioType of 'playback' or 'capture'");
  endif

  devicedata = strsplit(devicedata, "\n");

  card = "";
  dev = "";
  name = "";

  for i = 1:length(devicedata)
    l = devicedata{i};

    if strncmp(l, "card: ", 6)
      card = l(7:end);
    elseif strncmp(l, "device: ", 8)
      dev = l(9:end);
    elseif strncmp(l, "name: ", 6)
      name = l(7:end);
    elseif strncmp(l, "subclass: ", 10)
      info = {};
      info.Name = name;
      info.Device = [card ',' dev];
      # TODO: need populate these
      info.Channels = {};
      info.BitDepth = {};
      info.SamplingRate = {};
      
      devices{end+1} = info;
    endif

  endfor

endfunction

%!test
%! pi = raspi("raspberrypi.local");
%! devs = listAudioDevices(pi, "capture");
%! devs = listAudioDevices(pi, "playback");
%! fail ("listAudioDevices(pi, 'error')");
%! fail ("listAudioDevices(pi)");

%!error listAudioDevices();
%!error listAudioDevices(0);
