## Copyright (C) 2020 John D <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

classdef raspi < handle
  ## -*- texinfo -*- 
  ## @deftypefn {} {@var{retval} =} raspi ()
  ## @deftypefnx {} {@var{retval} =} raspi (@var{ipaddress})
  ## @deftypefnx {} {@var{retval} =} raspi (@var{ipaddress}, @var{username}, @var{password})
  ## @deftypefnx {} {@var{retval} =} raspi (@var{hostname}, @var{username}, @var{password})
  ## @deftypefnx {} {@var{retval} =} raspi (___, "Timeout", @var{time})
  ##
  ## Connect to raspberry pi board
  ##
  ## @subheading Inputs
  ## @var{ipaddress} - ip address that the raspberry pi is on.@*
  ## @var{hostname} - resolvable hostname that the raspberry pi is on.@*
  ## @var{username}, @var{password} - currently not used, but for matlab compatability.@*
  ## @var{timeout} - Optional timeout value (default 12.5) for communication to aRaspbeery Pi.@*
  ##
  ## @subheading Outputs
  ## @var{retval} - the connected raspi object.
  ##
  ## @end deftypefn

  properties (Access = private)
    config = {};
    connected = false;
    hwver = 0;
  endproperties

  properties (Access = public)
    Debug = false;
    Timeout = 0;
  endproperties

  properties (GetAccess = public, SetAccess = private)
    DeviceAddress = "";
    Port = 8888;
    BoardName = "";
    AvailableDigitalPins = [];
    AvailableLEDs = [];
    AvailableI2CBuses = [];
  endproperties

  methods (Access = public)
 
    function this = raspi (varargin)
      timeout = 12.5;

      propidx = 1;
      if nargin >= 1 && !strcmp(varargin{1}, "Timeout")
        host = varargin{1};
	propidx = 2;
      else
        host = "raspberrypi.local";
      endif
      port = 8888;
      this.DeviceAddress = resolvehost(host, 'address');

      if length(varargin) - propidx >= 0
        if mod (length(varargin)-propidx+1, 2) != 0
          error ("expected property value  or username password pair")
        endif
        if !ischar (varargin{propidx})
          error ("expected property name or username to be a string");
        endif
	if strcmp("Timeout", varargin{propidx})
	  timeout = varargin{propidx+1};
	endif
	propidx = propidx + 2;
      endif

      # another timeout?
      if length(varargin) - propidx >= 0
        if mod (length(varargin)-propidx+1, 2) != 0
          error ("expected property value")
        endif
        if !ischar (varargin{propidx})
          error ("expected property name to be a string");
        endif
	if strcmp("Timeout", varargin{propidx})
	  timeout = varargin{propidx+1};
	endif
	propidx = propidx + 2;
      endif

      this.connected = pigpio (this.DeviceAddress, port, timeout);

      this.hwver = get_hardware_revision(this.connected);
      this.config = raspberryio.getConfig(this.hwver);

      res = shell(this.connected, "raspi_init");
      if res != 0
        error ('Error running init script - %d', res); 
      endif
    endfunction

    function v = __playaudio__(this, devname, samplerate, data)
      v = 0;

      # create remote file
      dest = sprintf("/tmp/raspi-sys-%d-snd", getpid());
      mode = this.connected.FILE_WRITE;
      mode = bitor(mode, this.connected.FILE_CREATE);
      mode = bitor(mode, this.connected.FILE_TRUNC);

      rfd = file_open (this.connected, dest, mode);

      if rfd < 0
        error ("Couldnt open remote file %s", dest)
      endif

      unwind_protect 
        data = typecast(data, 'int8');
        bsize = 8192;

        for p = 1:bsize:length(data)
          if length(data) - p < bsize
            seg = data(p:end); 
          else
            seg = data(p:(p+bsize-1)); 
          endif

          if file_write (this.connected, rfd, seg) < 0
            error ("error writing to remote file");
          endif
        endfor

        file_close (this.connected, rfd);
        rfd = -1;

        # play
        chans = 1;
        args = sprintf("%d aplay --device='%s' --format=S16_LE --channels=%d --rate=%d %s", getpid, devname, chans, samplerate,  dest);
        v = shell (this.connected, "raspi_system", args);

      unwind_protect_cleanup
        if rfd > -1
          file_close (this.connected, rfd);
	endif

        try
          deleteFile(this, dest);
        catch err
          # do nothing
        end_try_catch
      end_unwind_protect

    endfunction

    function v = __servo__(this, pin, value)
      # value is ms
      v = set_servo_pulsewidth(this.connected, pin, value);
    endfunction

    # dispatch function for i2c
    function v = __i2c__ (this, cmd, varargin)
      switch cmd
        case "open"
          v = i2c_open(this.connected, varargin{:});
        case "close"
          v = i2c_close(this.connected, varargin{:});
        case "read"
          v = i2c_read_device(this.connected, varargin{:});
        case "write"
          v = i2c_write_device(this.connected, varargin{:});
        case "readregister"
          v = i2c_read_i2c_block_data(this.connected, varargin{:});
        case "writeregister"
          v = i2c_write_i2c_block_data(this.connected, varargin{:});
      endswitch
    endfunction

    # dispatch function for spi
    function v = __spi__ (this, cmd, varargin)
      switch cmd
        case "open"
          v = spi_open(this.connected, varargin{:});
        case "close"
          v = spi_close(this.connected, varargin{:});
        case "transfer"
          v = spi_xfer(this.connected, varargin{:});
      endswitch
    endfunction

    # dispatch function for serial
    function v = __serial__ (this, cmd, varargin)
      switch cmd
        case "open"
          v = serial_open(this.connected, varargin{:});
        case "close"
          v = serial_close(this.connected, varargin{:});
        case "read"
          v = serial_read(this.connected, varargin{:});
        case "write"
          v = serial_write(this.connected, varargin{:});
      endswitch
    endfunction

  endmethods

  methods (Access = private)
    function pins = get.AvailableDigitalPins (this)
      pins = [];
      #config_pins = this.get_group("digtial")
      config_pins = this.config.pins;
      type  ="digital";
      for i=1:numel (config_pins)
        idx = find (cellfun(@(x) strncmpi (x,  type, length (type)), config_pins{i}.modes), 1);
	if !isempty(idx)
          pins = [pins config_pins{i}.id];
	endif
      endfor
    endfunction

    function pins = get.AvailableLEDs (this)
      pins = {};
      config_pins = this.get_group("led");
      for i=1:numel (config_pins)
        pins{end+1} = config_pins{i}.name;
      endfor
    endfunction

    function pins = get.AvailableI2CBuses (this)
      pins = {};
      config_pins = this.get_group("i2c");
      for i=1:numel (config_pins)
	[id, pin] = sscanf(config_pins{i}.name, "i2c%d_%s", "C");
	if !isempty(id) && strcmp("sda", pin)
          pins{end+1} = sprintf("i2c-%d", id);
	endif
      endfor
    endfunction

    function v = get.BoardName (this)
        v = this.config.boardname;
    endfunction

    function set.Debug (this, value)
        this.connected.debug = value;
    endfunction

    function v = get.Debug (this, value)
        v = this.connected.debug;
    endfunction

    function set.Timeout (this, value)
        this.connected.timeout = value;
    endfunction

    function v = get.Timeout (this, value)
        v = this.connected.timeout;
    endfunction

    function info = get_pin (this, pin)
      info = [];

      idx = find (cellfun(@(x) (x.id == pin), this.config.pins), 1);

      if isempty (idx)
        error (["@raspi: unknown pin " pin]);
      endif

      info = this.config.pins{idx};
    endfunction

    function retval = get_group(this, type)
      retval = {};
      for i = 1:numel (this.config.pins)
        pininfo = this.config.pins{i};
        idx = find (cellfun(@(x) strncmpi (x, type, length (type)), pininfo.modes), 1);
        if !isempty(idx)
          #values = strsplit (pininfo.modes{idx}, "_");
          info = {};
          info.id = pininfo.id;
	  info.name = pininfo.modes{idx};
          #info.func = values{2};
          info.mode = pininfo.mode;
          info.owner = pininfo.owner;
          retval{end+1}= info;
        endif
      endfor
    endfunction

   function set_pin (this, pin, info)
      idx = find (cellfun(@(x) (x.id == pin), this.config.pins), 1);

      if isempty (idx)
        error ("@raspi: unknown pin");
      endif

      this.config.pins{idx} = info;
    endfunction

  endmethods
endclassdef

%!test
%! p = raspi();
%! assert(isa(p, "raspi"));
%! clear p

%!test
%! p = raspi('raspberrypi.local');
%! assert(isa(p, "raspi"));
%! assert(p.Timeout, 12.5);
%! assert(!isempty(p.BoardName));
%! assert(!isempty(p.DeviceAddress));
%! assert(!isempty(p.AvailableDigitalPins));
%! assert(p.Port, 8888);
%! assert(p.Timeout, 12.5);
%! assert(p.Debug, false);
%! clear p

%!test
%! p = raspi('raspberrypi.local', 'username', 'password');
%! assert(isa(p, "raspi"));
%! assert(p.Timeout, 12.5);
%! clear p

%!test
%! p = raspi('raspberrypi.local', 'Timeout', 13);
%! assert(isa(p, "raspi"));
%! assert(p.Timeout, 13);
%! clear p

%!test
%! p = raspi('raspberrypi.local', 'username', 'password', 'Timeout', 14);
%! assert(isa(p, "raspi"));
%! assert(p.Timeout, 14);
%! clear p

%!test
%! p = raspi('Timeout', 13);
%! assert(isa(p, "raspi"));
%! assert(p.Timeout, 13);
%! clear p
