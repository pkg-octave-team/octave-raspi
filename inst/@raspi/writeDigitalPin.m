## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} writeDigitalPin (@var{obj}, @var{pin}, @var{value})
## Write digital value to a digital I/O pin.
##
## @subsubheading Inputs
## @var{pi} - connected raspi object.
##
## @var{pin} - pin number to write to.
##
## @var{value} - the logical value (0, 1, true false) to write to the pin.
##
## If pin was unconfigured before using, pin is set into digital mode.
##
## @subsubheading Example
## @example
## @code{
## a = raspi();
## writeDigitalPin(a,17,1);
## }
## @end example
##
## @seealso{raspi, readDigitalPin}
##
## @end deftypefn

function writeDigitalPin (obj, pin, value)
  if nargin != 3
    error ("@raspi.writeDigitalPin: expected pin and value");
  endif
  if !isnumeric(pin)
    error ("@raspi.writeDigitalPin: expected numeric pin");
  endif
  if (!isnumeric(value) && !islogical(value)) || (value != 1 && value != 0)
    error ("@raspi.writeDigitalPin: expected value as logical or 0 or 1");
  endif  

  pininfo = obj.get_pin(pin);
  if !strncmp("digital", pininfo.mode, 7)
    configurePin(obj, pin, "digitaloutput");
  endif
 
  res = gpio_write(obj.connected, pin, int32(value));

  if res != 0
    error ("could not write pin %d - %d - %s", pin, res, error_text(obj.connected, res));
  endif
  
endfunction

%!shared pi
%! pi = raspi("raspberrypi.local");

%!test
%! writeDigitalPin(pi, 4, 1);
%! writeDigitalPin(pi, 4, 0);
%! writeDigitalPin(pi, 4, false);
%! writeDigitalPin(pi, 4, true);

%!error <undefined> writeDigitalPin()

%!error <expected pin and value> writeDigitalPin(pi)

%!error writeDigitalPin(pi, 2, 1, 1)

%!error <expected numeric pin> writeDigitalPin(pi, "nopin", 1)
