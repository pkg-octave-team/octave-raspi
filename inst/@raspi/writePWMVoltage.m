## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} writePWMVoltage (@var{ar}, @var{pin}, @var{voltage})
## Emulate an approximate voltage out of a pin using PWM.
##
## @subsubheading Inputs
## @var{ar} - connected raspi object
##
## @var{pin} - pin to write to.
##
## @var{voltage} - voltage to emulate with PWM, between 0 - 5.0
##
## @subsubheading Example
## @example
## @code{
## a = raspi();
## writePWMVoltage(a,17,1.0);
## }
## @end example
##
## @seealso{raspi, writePWMDutyCycle}
## @end deftypefn

function writePWMVoltage (obj, pin, value)
  if nargin < 3
    error ("@raspi.writePWMVoltage: expected pin name and value");
  endif

  # TODO: need look at board type for what voltage range is allowed
  # and convert
  maxvolts = 3.3;
  if !isnumeric(value) || value < 0 || value > maxvolts
    error('writePWMVoltage: value must be between 0 and %f', maxvolts);
  endif

  # assuming here for now 0 .. 3.3 is linear to 0 - 100% pwm
  val = value/maxvolts;
  writePWMDutyCycle(obj, pin, val);
  
endfunction

%!shared ar
%! ar = raspi("raspberrypi.local");

%!test
%! writePWMVoltage(ar, 4, 3.3);

%!error <undefined> writePWMVoltage();

%!error <expected> writePWMVoltage(ar)

%!error <expected pin> writePWMVoltage(ar, 4)

%!error <unknown pin> writePWMVoltage(ar, -1, 1)

%!error <value must be between> writePWMVoltage(ar, 4, -1)

%!error <value must be between> writePWMVoltage(ar, 4, 5.1)

%!test
%! writePWMVoltage(ar, 4, 0.0);
