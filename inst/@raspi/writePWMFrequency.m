## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} writePWMFrequency (@var{ar}, @var{pin}, @var{freq})
## Set pin to output a square wave with a specified frequency
##
## @subsubheading Inputs
## @var{ar} - connected raspi object
##
## @var{pin} - pin to write to.
##
## @var{frequency} - n.
##
## @seealso{raspi, writePWMVoltage, writePWMFrequency}
##
## @end deftypefn

function writePWMFrequency (obj, pin, value)
  if nargin < 3
    error ("@raspi.writePWMFrequency: expected pin name and value");
  endif
  if !isnumeric(pin)
    error ("@raspi.writePWMFrequency: expected numeric pin");
  endif
  if (!isnumeric(value) || value < 0)
    error ("@raspi.writePWMFrequency: expected value greater than 0");
  endif  

  pininfo = obj.get_pin(pin);
  if !strcmp("pwm", pininfo.mode)
    configurePin(obj, pin, "pwm");
  endif
 
  set_PWM_frequency(obj.connected, pin, value);
  
endfunction

%!shared ar
%! ar = raspi("raspberrypi.local");

%!test
%! writePWMFrequency(ar, 4, 2000);

%!error <undefined> writePWMFrequency();

%!error <expected> writePWMFrequency(ar)

%!error <expected pin> writePWMFrequency(ar, 4)

%!error <unknown pin> writePWMFrequency(ar, -1, 1000)

%!error <expected value greater> writePWMFrequency(ar, 4, -1)

%!test
%! # ensure pwm off
%! writePWMDutyCycle(ar, 4, 0);
