## Copyright (C) 2020 John D <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

classdef servo
  ## -*- texinfo -*- 
  ## @deftypefn {} {@var{retval} =} servo (@var{piobj}, @var{pin})
  ## @deftypefnx {} {@var{retval} =} servo (@var{piobj}, @var{pin}, @var{propertyname}, @var{propertyvalue} ...)
  ##
  ## Create a servo object on the specified raspberry pi board pin.
  ##
  ## @subheading Inputs
  ## @var{piobj} - a connected raspberry pi raspi object.@*
  ## @var{pin} - the GPIO pin number the servo is connected to.@*
  ## @var{propertyname}, @var{propertyvalue} - property name/value pairs for additional servo properties
  ##
  ## Known properties are:
  ## @table @asis
  ## @item MinPulseDuration
  ## The minimum pulse duration in seconds. 
  ## @item MaxPulseDuration
  ## The maximum pulse duration in seconds. 
  ## @end table
  ##
  ## @subheading Outputs
  ## @var{retval} - returns a servo object.
  ##
  ## @subheading Examples
  ## Create a servo object with min and max pulses on pin 17.
  ##
  ## @example
  ## p = raspi("raspberrypi.local");
  ## s = servo(p, 17, 'MinPulseDuration', 1e-3, 'MaxPulseDuration', 2e-3)
  ## @end example
  ##
  ## @seealso{raspi}
  ## @end deftypefn
  ##
  ## @deftypefn {} {} writePosition (@var{servoobj}, @var{angle})
  ## Command the servo to an angle value.
  ##
  ## @subheading Inputs
  ## @var{servoobj} - the servo object.@*
  ## @var{angle} - the angle ( 0 - 180) to command the servo to.
  ##
  ## The angle is translated 0 being the min pulse width value and 180 being
  ## the max pulse width value.
  ##
  ## @subheading Outputs
  ## None
  ## 
  ## @seealso{servo}
  ## @end deftypefn

  properties (GetAccess = public, SetAccess = private)
    Parent = [];
    Pins = [];
    MinPulseDuration = 5.44e-4;
    MaxPulseDuration = 2.4e-3;
  endproperties

  methods
    function this = servo (p, pin, varargin)
      this.Parent = p;
      this.Pins = pin;

      if mod (length(varargin), 2) != 0
        error ("expected property value pairs")
      endif
      if !iscellstr (varargin (1:2:length(varargin)))
        error ("expected property names to be strings");
      endif

      for i=1:2:length(varargin)
        name = tolower(varargin{i});
        value = varargin{i+1};
	if !isnumeric(value)
          error ("Expected property to be numeric");
	endif
        switch (name)
          case "minpulseduration"
            this.MinPulseDuration = value;
          case "maxpulseduration"
            this.MaxPulseDuration = value;
          otherwise
            error ("Unknown property name");
	endswitch
      endfor

      if this.MinPulseDuration*1000000 < 500
        error ("MinPulseDuration is too small");
      elseif this.MaxPulseDuration*1000000 > 25000
        error ("MaxPulseDuration is too big");
      elseif this.MinPulseDuration >= this.MaxPulseDuration
        error ("MaxPulseDuration <= MinPulseDuration")
      endif

      # configure
      configurePin(this.Parent, this.Pins, "pwm");

      nok = __servo__(this.Parent, this.Pins, 0);
      if nok
        error ("Initializing servo");
      endif
    endfunction

    function writePosition(this, pos)
      if !isnumeric(pos) || pos < 0 || pos > 180
        error ("pos should be value between 0 .. 180");
      endif

      # get angle as a ms value
      if pos == 0
        value = this.MinPulseDuration*1000000;
      else
        range = this.MaxPulseDuration*1000000 - this.MinPulseDuration*1000000;
        value = this.MinPulseDuration*1000000 + (range * (pos/180.0));
      endif
      nok = __servo__(this.Parent, this.Pins, value);

      if nok
        error ("Setting position");
      endif
    endfunction

    function disp (this)
      printf("  %s object with properties of:\n", class(this));
      printf("                Pins = %d\n", this.Pins);
      printf("    MinPulseDuration = %f\n", this.MinPulseDuration);
      printf("    MaxPulseDuration = %f\n", this.MaxPulseDuration);
    endfunction
  endmethods

endclassdef

%!test
%! pi = raspi("raspberrypi.local");
%! s = servo(pi, 17, 'MinPulseDuration', 1e-3, 'MaxPulseDuration', 2e-3);
%! assert(s.MinPulseDuration, 1e-3);
%! assert(s.MaxPulseDuration, 2e-3);
%! assert(s.Pins, 17);
%! writePosition(s, 90);
%! clear s
