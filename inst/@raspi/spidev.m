## Copyright (C) 2020 John D <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

classdef spidev
  ## -*- texinfo -*- 
  ## @deftypefn {} {@var{retval} =} spidev (@var{piobj}, @var{channel})
  ## @deftypefnx {} {@var{retval} =} spidev (@var{piobj}, @var{channel}, @var{mode}, @var{speed}, @var{bitsperword})
  ##
  ## Create a spi object on the specified raspberry pi board pin.
  ##
  ## @subheading Inputs
  ## @var{piobj} - a connected raspberry pi raspi object.@*
  ## @var{channel} - channel name 'ce0' or 'ce1' or number  0 or 1.@*
  ## @var{mode} - the spi mode.@*
  ## @var{speed} - spi speed.@*
  ## @var{bitsperword} - spi speed.@*
  ##
  ## @subheading Outputs
  ## @var{retval} - returns a spidev object.
  ##
  ## @subheading Object Properties
  ## Known properties are:
  ## @table @asis
  ## @item Channel
  ## The channel used. channel name 'CE1' or 'CE1'.
  ## @item Mode
  ## SPI mode used.
  ## @item Speed
  ## SPI speed.
  ## @item BitsPerWord
  ## Bits per word used for read write of data
  ## @item Parent
  ## Raspi object of the spidev.
  ## @end table
  ##
  ## @seealso{raspi}
  ## @end deftypefn
  ##
  ## @deftypefn {} {@var{outdata} =} writeRead (@var{spiobj}, @var{data})
  ## Write data to SPI and read back data of same length.
  ##
  ## @subheading Inputs
  ## @var{spiobj} - the spidev object.@*
  ## @var{data} - the data to write to the device.
  ##
  ## @subheading Outputs
  ## @var{outdata} - data read from device in  response to write.
  ##
  ## @seealso{spidev}
  ## @end deftypefn

  properties (GetAccess = public, SetAccess = private)
    Parent = [];
    Pins = [];
    Channel = [];
    Mode = 0;
    Speed = 0;
    BitsPerWord = 8;
  endproperties

  properties (Access = private)
    handle = [];
  endproperties

  methods
    function this = spidev (p, channel, mode, speed, bitsperword)
      this.Parent = p;

      #this.Pins = pin;
      if nargin < 2
        error ("expected raspi and channel");
      endif
      if nargin < 3
        mode = 0;
      endif
      if nargin < 4
        speed = 200000;
      endif
      if nargin < 5
        bitsperword = 8;
      endif

      if ischar(channel)
        # channel will be ce0 or ce1 etc
        channel = sscanf(tolower(channel), "ce%d");
	if isempty(channel)
          error ("spidev: unknown channel");
	endif
      elseif !isnumeric (channel) || channel < 0 || channel > 1
        error ('spidev: expected channel to be numeric 0 or 1 OR ce0, ce1');
      endif

      # TODO: what flags need set for the spi ?

      this.Channel = channel;
      this.Speed = speed;
      this.Mode = mode;
      this.BitsPerWord = bitsperword;

      this.handle = __spi__(this.Parent, "open", this.Channel, this.Speed, this.Mode);
      if this.handle < 0
        error("spidev: couldnt open spi - %d", this.handle);
      endif
    endfunction

    function delete (this)
      try
        p = this.Parent;
        __spi__(p, "close", this.handle);
	this.Parent = [];
      catch
        # do nothing
      end_try_catch
    endfunction

    function data = writeRead(this, data)
      data = __spi__(this.Parent, "transfer", this.handle, data);
    endfunction

    function disp (this)
      printf("  %s object with properties of:\n", class(this));
      printf("          Channel = CE%d\n", this.Channel);
      printf("             Mode = %d\n", this.Mode);
      #printf("           Speed = %d\n", this.Speed);
    endfunction
  endmethods

endclassdef

%!test
%! pi = raspi("raspberrypi.local");
%! s = spidev(pi, "ce0", 0);
%! assert(s.Channel, 0);
%! assert(s.Mode, 0);
%! assert(s.Speed, 200000);
%! assert(s.BitsPerWord, 8);
%! clear s
