## Copyright (C) 2020 John D <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

classdef serialdev
  ## -*- texinfo -*- 
  ## @deftypefn {} {@var{retval} =} serialdev (@var{piobj}, @var{port})
  ## @deftypefnx {} {@var{retval} =} serialdev (@var{piobj}, @var{port}, @var{baudrate}, @var{databits}, @var{parity}, @var{stopbits})
  ##
  ## Create a serial object on the specified raspberry pi port.
  ##
  ## @subheading Inputs
  ## @var{piobj} - a connected raspberry pi raspi object.@*
  ## @var{port} - The name of the serial port to connect to for example, '/dev/serial0'.@*
  ## @var{baudrate} - the baudrate (default 115200).@*
  ## @var{databits} - the number of data bits 5,6,7,8 (default), 9.@*
  ## @var{parity} - parity of 'none' (default), 'odd' or 'even'.@*
  ## @var{stopbits} - number of stop bits 1 (default) or 2.@*
  ##
  ## @subheading Outputs
  ## @var{retval} - returns a serialdev object.
  ##
  ## @subheading Object Properties
  ## Known properties are:
  ## @table @asis
  ## @item Port
  ## The port used (read only).
  ## @item BaudRate
  ## Baud rate used (readonly).
  ## @item DataBits
  ## Databits used (readonly).
  ## @item Parity
  ## Parity used (readonly).
  ## @item StopBits
  ## Stopbits used (readonly).
  ## @item Timeout
  ## Timeout value for read.
  ## @item Parent
  ## Rasppi object of the serialdev. (readonly)
  ## @end table
  ##
  ## @seealso{raspi}
  ## @end deftypefn
  ##
  ## @deftypefn {} {} write (@var{serialobj}, @var{data})
  ## @deftypefnx {} {} write (@var{serialobj}, @var{data}, @var{precision})
  ## Write @var{data} using the @var{precision} datatype.
  ##
  ## @subheading Inputs
  ## @var{serialobj} - the serialdev object.@*
  ## @var{data} - the data to write.
  ## @var{precision} - the data type to convert from when writing.
  ##
  ## @subheading Outputs
  ## None
  ##
  ## @seealso{serialdev}
  ## @end deftypefn
  ##
  ## @deftypefn {} {@var{outdata} =} read (@var{serialobj}, @var{count})
  ## @deftypefnx {} {@var{outdata} =} read (@var{serialobj}, @var{count}, @var{precision})
  ## Read at most @var{count} values using the @var{precision} datatype.
  ##
  ## @subheading Inputs
  ## @var{serialobj} - the serialdev object.@*
  ## @var{count} - the number of values to read.
  ## @var{precision} - the precision of the data to read.
  ##
  ## @subheading Outputs
  ## @var{outdata} - data read from device.
  ##
  ## @seealso{serialdev}
  ## @end deftypefn

  properties (GetAccess = public, SetAccess = private)
    Parent = [];
    Pins = [];
    Port = [];
    BaudRate = 115200;
    DataBits = 8;
    Parity = 'none';
    StopBits = 1;
  endproperties

  properties (Access = public)
    TimeOut = 10;
  endproperties

  properties (Access = private)
    handle = [];
  endproperties

  methods
    function this = serialdev (p, port, baudrate, databits, parity, stopbits)
      this.Parent = p;

      #this.Pins = pin;
      if nargin < 2
        error ("expected raspi object and port");
      endif

      if !ischar(port)
        error ("Expected port to be a string");
      endif
      this.Port = port;

      if nargin > 2
        if !isnumeric(baudrate) || baudrate < 50
          error ("Invalid baudrate value");
	endif
	this.BaudRate = baudrate;
      endif

      # pigpio doesnt support set up of databits etc

      this.handle = __serial__(this.Parent, "open", this.Port, this.BaudRate, 0);
      if this.handle < 0
        error("Couldnt open serial - %d", this.handle);
      endif
    endfunction

    function delete (this)
      try
        p = this.Parent;
        __serial__(p, "close", this.handle);
	this.Parent = [];
      catch
        # do nothing
      end_try_catch
    endfunction

    function numwrote = write(this, data, precision)
      if (nargin < 2)
        error ("Expected data");
      elseif (nargin < 3)
        precision = [];
      endif

      switch (precision)
        case {"char" "schar" "int8"}
          data = int8 (data);
        case {"uchar" "uint8"}
          data = uint8 (data);
        case {"int16" "short"}
          data = int16 (data);
        case {"uint16" "ushort"}
          data = uint16 (data);
        case {"int32" "int"}
          data = int32 (data);
        case {"uint32" "uint"}
          data = uint32 (data);
        case {"long" "int64"}
          data = int64 (data);
        case {"ulong" "uint64"}
          data = uint64 (data);
        case {"single" "float" "float32"}
          data = single (data);
        case {"double" "float64"}
          data = double (data);
        case []
          %% use data as it is
        otherwise
        error ("precision not supported");
      endswitch

      numwrote = __serial__(this.Parent, "write", this.handle, typecast(data,'uint8'));
    endfunction

    function data = read(this, count, precision)

      if nargin < 2
        error ("Expected count");
      endif

      if nargin < 3
        precision = "uint8";
      endif

      toread = count;

      switch (precision)
        case {"char" "schar" "int8"}
          toclass = "int8";
          tosize = 1;
        case {"uchar" "uint8"}
          toclass = "uint8";
          tosize = 1;
        case {"int16" "short"}
          toclass = "int16";
          tosize = 2;
          toread = toread * 2;
        case {"uint16" "ushort"}
          toclass = "uint16";
          tosize = 2;
        case {"int32" "int"}
          toclass = "int32";
          tosize = 4;
          toread = toread * 4;
        case {"uint32" "uint"}
          toclass = "uint32";
          tosize = 4;
          toread = toread * 4;
        case {"long" "int64"}
          toclass = "int64";
          toread = toread * 8;
          tosize = 8;
        case {"ulong" "uint64"}
          toclass = "uint64";
          toread = toread * 8;
          tosize = 8;
        case {"single" "float" "float32"}
          toclass = "single";
          tosize = 4;
          toread = toread * 4;
        case {"double" "float64"}
          toclass = "double";
          tosize = 8;
          toread = toread * 8;
        otherwise
          error ("precision not supported");
      endswitch
 
      # TODO: do we need to loop here?
      data = __serial__(this.Parent, "read", this.handle, toread);

      data = typecast(data,toclass);
    endfunction

    function disp (this)
      printf("  %s object with properties of:\n", class(this));
      printf("             Port = %s\n", this.Port);
      printf("         Baudrate = %d\n", this.BaudRate);
      printf("         DataBits = %d\n", this.DataBits);
      printf("           Parity = %s\n", this.Parity);
      printf("         StopBits = %d\n", this.StopBits);
    endfunction
  endmethods

endclassdef
