## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} display (@var{pi})
## Display the raspi object in a verbose way
##
## @subsubheading Inputs
## @var{pi} - the raspi object.
##
## @seealso{raspi}
## @end deftypefn

function display (this)

  printf ("%s = \n", inputname (1));
  printf ("  %s object with fields of: \n", class(this));
  printf ("    DeviceAddress = %s\n", this.DeviceAddress);
  printf ("             Port = %d\n", this.Port);
  printf ("        BoardName = %s\n", this.BoardName);

endfunction
