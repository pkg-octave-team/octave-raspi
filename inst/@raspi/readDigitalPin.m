## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {@var{value} =} readDigitalPin (@var{obj}, @var{pin})
## Read digital value from a digital I/O pin.
##
## @subsubheading Inputs
## @var{ar} - connected raspi object.
##
## @var{pin} - string name of the pin to read.
##
## @subsubheading Outputs
## @var{value} - the logical value (0, 1, true false) of the current pin state.
##
## @subsubheading Example
## @example
## @code{
## pi = raspi ();
## pinvalue = readDigitalPin (pi, 17);
## }
## @end example
##
## @seealso{raspi, writeDigitalPin}
## @end deftypefn

function retval = readDigitalPin (obj, pin)
  if nargin != 2
    error ("@raspi.readDigitalPin: expected pin");
  endif
  if !isnumeric(pin)
    error ("@raspi.readDigitalPin: expected pin number");
  endif

  pininfo = obj.get_pin(pin);
  if !strncmp("digital", pininfo.mode, 7)
    configurePin(obj, pin, "digitalinput");
  endif
  
  retval = gpio_read(obj.connected, pin);
endfunction

%!shared pi
%! pi = raspi("raspberrypi.local");

%!test
%! readDigitalPin(pi, 4);

%!error <undefined> readDigitalPin()

%!error <expected pin> readDigitalPin(pi)

%!error readDigitalPin(pi, 4, 2)

%!error <expected pin number> readDigitalPin(pi, "nopin")
