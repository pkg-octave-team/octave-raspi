## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} getFile (@var{obj}, @var{filename})
## @deftypefnx {} {} getFile (@var{obj}, @var{filename}, @var{destination})
## Transfer a file from the raspberry pi to local computer.
##
## @subsubheading Inputs
## @var{obj} - connected raspi object.
##
## @var{filename} - file name to get.
##
## @var{destination} - destination to save the file. If destination is a directory, the file will be saved to the directory,
## otherwise the file will be saved to the destination filename.
##
## @seealso{raspi, putFile}
##
## @end deftypefn

function getFile (obj, filename, dest)

  if nargin < 2
    error ("Expected filename");
  endif

  if ! ischar (filename)
    error ("filename should be a string");
  endif

  if nargin < 3
    dest = ""
  elseif !ischar (dest)
    error ("expected destination to be a string");
  endif

  [fpath, fname, fext] = fileparts (filename);
  fname = [fname fext];

  if !isempty(dest)
    if isfolder(dest)
      dest = fullfile(dest, fname);
    endif
  else
    dest = fname;
  endif

  rfd = file_open (obj.connected, filename, obj.connected.FILE_READ);
  if (rfd >= 0)
    unwind_protect 

      # create local file
      lfd = fopen (dest, "wb"); 

      if lfd < 0
        error ("Couldnt open local file %s", dest)
      endif

      unwind_protect 
        while true
          rd = file_read (obj.connected, rfd, 1024);
          if !isempty (rd)
            if fwrite (lfd, rd) < length (rd)
              error ("error writing to local file");
	    endif
          else
            break;
          endif
        endwhile

      unwind_protect_cleanup
        fclose (lfd);
      end_unwind_protect

    unwind_protect_cleanup
      file_close(obj.connected, rfd);
    end_unwind_protect
  else
    error ("Couldnt open remote file '%s", filename);
  endif

endfunction

%!test
%! pi = raspi("raspberrypi.local");
%! tmp = tempname();
%! getFile(pi, "/home/pi/.profile", tmp);
%! assert(exist(tmp, "file") == 2);
%! delete(tmp);
