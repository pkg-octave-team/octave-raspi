## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} writeLED (@var{obj}, @var{led}, @var{value})
## Control an on board led.
##
## @subsubheading Inputs
## @var{obj} - connected raspi object.
##
## @var{led} - string name of the led to write to.
##
## @var{value} - the logical value (0, 1, true false) to write to the led.
##
## @seealso{raspi, showLEDs, writeDigitalPin}
##
## @end deftypefn

function writeLED (obj, led, value)

  if nargin != 3
    error ("@raspi.writeLED: expected LED name and value");
  endif

  if ischar (led)
    xled = sscanf (tolower(led), "led%d");

    if isempty (xled)
      error  ("unknown LED %s", led);
    else
      led = xled;
    endif
  elseif !isnumeric (led) || led < 0 || led > 1
    error ("@raspi.writeLED: expected led name");
  endif

  if (!isnumeric (value) && !islogical (value)) || (value != 1 && value != 0)
    error ("@raspi.writeLED: expected value as logical or 0 or 1");
  endif  

  # gpio way
  %{
  # io logic is 0 = led on
  if value
    value = 0;
  else
    value = 1;
  endif

  pin = obj.get_group (sprintf("led%d", led));
  if length (pin) != 1
    error ("Couldnt find led%d", led);
  endif
  res = gpio_write (obj.connected, pin{1}.id, value);
  if res != 0
    error ("could not set led - %d - %s", res, error_text(obj.connected, res));
  endif
  %}
  # script way
  res = shell(obj.connected, "raspi_led", sprintf("%d %d", led, value));
  if res == 1
    error ("Unknown LED led%d", led); 
  elseif res < 0
    error ('Error running led script - %d : %s', res, error_text(obj.connected, res)); 
  endif

endfunction

%!shared pi
%! pi = raspi("raspberrypi.local");

%!xtest
%! writeLED(pi,"led0", 1);
%! writeLED(pi,"led0", 0);
%! writeLED(pi,"led0", false);
%! writeLED(pi,"led0", true);

%!error <undefined> writeLED()

%!error <expected LED name and value> writeLED(pi)

%!error writeLED(pi, "led0", 1, 1)

%!error <unknown LED noled> writeLED(pi, "noled", 1)
