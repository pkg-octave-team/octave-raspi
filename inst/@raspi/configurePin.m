## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{currmode} =} configurePin (@var{pi}, @var{pin})
## @deftypefnx {} {} configurePin (@var{obj}, @var{pin}, @var{mode})
## Set/Get pin mode for a specified pin on a raspi connection.
##
## configurePin (@var{pi}, @var{pin}) will get the current mode of the specified pin.
##
## configurePin (@var{pi}, @var{pin}, @var{mode}) will attempt set the pin to the specified
## mode if the mode is unset.
##
## @subsubheading Inputs
## @var{pi} - the raspi object of the connection to an raspi board.
##
## @var{pin} - string name of the pin to set/get the mode of.
##
## @var{mode} - string mode to set the pin to.
##
## @subsubheading Outputs
## @var{mode} - string current mode of the pin.
##
## Valid modes can be:
## @itemize @bullet
## @item DigitalInput
## - Acquire digital signals from pin
## @item DigitalOutput
## - Generate digital signals from pin
## @item PWM
## - Specify pin to use a pulse width modulator
## @item Unset
## - Clears pin designation. The pin is no longer reserved and can be automatically
## set at the next operation.
## @end itemize
##
## @seealso{raspi}
##
## @end deftypefn

function retval = configurePin (this, pin, mode)

  if nargin != 2 && nargin != 3 
    error ("@raspi.configurePin: expected pin name and value");
  endif

  #if !ischar (pin) && !isnumeric (pin)
  #  error ("@raspi.configurePin: expected pin name as string");
  #endif
  
  if !isnumeric (pin)
    error ("@raspi.configurePin: expected pin name numeric");
  endif  
  
  pininfo = this.get_pin (pin);
  
  if nargin == 3
    % set mode
    mode = tolower(mode);

    if !ischar (mode)
      error ("@raspi.configurePin: expected pin mode as string");
    endif

    modeclass = "";
    pigmode = 0;
    switch (mode)
      case "unset"
	modeclass = "";
	pigmode = 0;
      case "digitalinput"
        modeclass = "digital";
	pigmode = 0;
      case "digitaloutput"
        modeclass = "digital";
	pigmode = 1;
      case "pwm"
        modeclass = "pwm";
	pigmode = 1;
      otherwise
        error("Unknown mode '%s'", mode);
    endswitch

    if !strcmp(mode, "unset")
      idx = find (cellfun(@(x) strncmpi (x, modeclass, length (modeclass)), pininfo.modes), 1);
      if isempty(idx)
        error("Invalid mode change for this pin");
      endif
    endif

    if !strcmp(pininfo.mode, "unset") && length(modeclass) > 0 && !strncmp (modeclass, pininfo.mode, length(modeclass))
      error("Incompatable mode change of %s to %s", pininfo.mode, mode);
    endif

    ok = 0;
    if strcmp(mode, "unset")
      # turn off potential pwm
      if strcmp(pininfo.mode, "pwm")
        ok = set_servo_pulsewidth(this.connected, pin, 0);
      endif
    else
      ok = set_mode(this.connected, pin, pigmode);
    endif
    
    if ok != 0
      error ("Couldnt set mode");  
    endif

    pininfo.mode = mode;
    this.set_pin (pin, pininfo);
  
  else
    retval = pininfo.mode;
  endif
  
endfunction

%!test
%! pi = raspi("raspberrypi.local");
%! assert(!isempty(pi));
%! configurePin(pi, 4, "digitaloutput");
%! assert(configurePin(pi, 4), "digitaloutput");
%! configurePin(pi, 4, "unset");
%! assert(configurePin(pi, 4), "unset");
