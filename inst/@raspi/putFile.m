## Copyright (C) 2020 John Donoghue <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## -*- texinfo -*- 
## @deftypefn {} {} putFile (@var{obj}, @var{filename})
## @deftypefnx {} {} putFile (@var{obj}, @var{filename}, @var{destination})
## Transfer a file from local computer to the raspberry pi.
##
## @subsubheading Inputs
## @var{obj} - connected raspi object.
##
## @var{filename} - file name to transfer.
##
## @var{destination} - destination to save the file. 
##
## @seealso{raspi, getFile}
##
## @end deftypefn

function putFile (obj, filename, dest)

  if nargin < 2
    error ("Expected filename");
  endif

  if ! ischar (filename)
    error ("filename should be a string");
  endif

  if nargin < 3
    dest = ""
  elseif ! ischar (dest)
    error ("expected destination to be a string");
  endif

  [fpath, fname, fext] = fileparts (filename);
  fname = [fname fext];

  if isempty (dest)
    dest = fname;
  endif

  lfd = fopen (filename, "rb"); 
  if (lfd >= 0)
    unwind_protect 

      # create remote file
      mode = obj.connected.FILE_WRITE;
      mode = bitor(mode, obj.connected.FILE_CREATE);
      mode = bitor(mode, obj.connected.FILE_TRUNC);

      rfd = file_open (obj.connected, dest, mode);

      if rfd < 0
        error ("Couldnt open remote file %s", dest)
      endif

      unwind_protect 
        while !feof (lfd)
          rd = fread (lfd, 1024);
          if !isempty (rd)
            if file_write (obj.connected, rfd, rd) < 0
              error ("error writing to remote file");
	    endif
          else
            break;
          endif
        endwhile

      unwind_protect_cleanup
        file_close (obj.connected, rfd);
      end_unwind_protect

    unwind_protect_cleanup
      fclose (lfd);
    end_unwind_protect
  else
    error ("Couldnt open local file '%s", filename);
  endif

endfunction

%!test
%! pi = raspi("raspberrypi.local");
%! tmp = tempname();
%! rmtmp = sprintf("/tmp/raspi-test-%d", getpid());
%! # create data in temp file
%! fd = fopen(tmp, "w");
%! fprintf(fd, "hello\n");
%! fclose(fd);
%! # put to remote and dlete local file
%! putFile(pi, tmp, rmtmp);
%! delete(tmp);
%! assert(exist(tmp, "file") == 0);
%!
%! # now get remote file
%! getFile(pi, rmtmp, tmp);
%! assert(exist(tmp, "file") == 2);
%! delete(tmp);
%! 
%! # delete remote file
%! deleteFile(pi, rmtmp);
