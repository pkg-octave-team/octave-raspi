## Copyright (C) 2020 John D <john.donoghue@ieee.org>
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

classdef i2cdev
  ## -*- texinfo -*- 
  ## @deftypefn {} {@var{retval} =} i2cdev (@var{piobj}, @var{busnum}, @var{address})
  ## Create an I2C object on the specified raspberry pi board pin.
  ##
  ## @subheading Inputs
  ## @var{piobj} - a connected raspberry pi raspi object.@*
  ## @var{busnum} - the I2C bus 0 or 1 or bus name 'i2c-0' or 'i2c-1'.@*
  ## @var{address} - the I2C device address.@*
  ##
  ## Known properties are:
  ## @table @asis
  ## @item Bus
  ## The Bus the device is on. (read only)
  ## @item I2CAddress
  ## The I2C address the device is om (read only)
  ## @end table
  ##
  ## @subheading Outputs
  ## @var{retval} - returns a i2cdev object.
  ##
  ## @seealso{raspi}
  ## @end deftypefn
  ##
  ## @deftypefn {} {} write (@var{i2cobj}, @var{data})
  ## @deftypefnx {} {} write (@var{i2cobj}, @var{data}, @var{dataprecision})
  ## Write data to a i2cdev object.
  ##
  ## @subheading Inputs
  ## @var{i2cobj} - the i2cdev object.@*
  ## @var{data} - data to write to the i2cdev object.
  ## @var{dataprecision} - data to precision for the input data.
  ##
  ## @subheading Outputs
  ## None
  ##
  ## @seealso{i2cdev, read}
  ## @end deftypefn
  ##
  ## @deftypefn {} {@var{data} =} read (@var{i2cobj}, @var{cnt})
  ## @deftypefnx {} {@var{data} =} read (@var{i2cobj}, @var{cnt}, @var{dataprecision})
  ## Read data from a i2cdev object.
  ##
  ## @subheading Inputs
  ## @var{i2cobj} - the i2cdev object.@*
  ## @var{cnt} - number of values to read from device.
  ## @var{dataprecision} - data to precision for the data.
  ##
  ## @subheading Outputs
  ## @var{data} - the data read from the device
  ##
  ## @seealso{i2cdev, write}
  ## @end deftypefn
  ##
  ## @deftypefn {} {} writeRegister (@var{i2cobj}, @var{register}, @var{data})
  ## @deftypefnx {} {} write (@var{i2cobj}, @var{register}, @var{data}, @var{dataprecision})
  ## Write data to a register on a i2cdev object.
  ##
  ## @subheading Inputs
  ## @var{i2cobj} - the i2cdev object.@*
  ## @var{register} - register to write the data to on the i2cdev object.@*
  ## @var{data} - data to write to the i2cdev object.
  ## @var{dataprecision} - data to precision for the input data.
  ##
  ## @subheading Outputs
  ## None
  ##
  ## @seealso{i2cdev, readRegsiter}
  ## @end deftypefn
  ##
  ## @deftypefn {} {@var{data} =} readRegister (@var{i2cobj}, @var{register}, @var{cnt})
  ## @deftypefnx {} {@var{data} =} readRegister (@var{i2cobj}, @var{register}, @var{cnt}, @var{dataprecision})
  ## Read data from a register on a i2cdev object.
  ##
  ## @subheading Inputs
  ## @var{i2cobj} - the i2cdev object.@*
  ## @var{register} - register to read the data from on the i2cdev object.@*
  ## @var{cnt} - number of values to read from device.
  ## @var{dataprecision} - data to precision for the data.
  ##
  ## @subheading Outputs
  ## @var{data} - the data read from the device
  ##
  ## @seealso{i2cdev, write}
  ## @end deftypefn

  properties (GetAccess = public, SetAccess = private)
    Parent = [];
    Pins = [];
    I2CAddress = [];
    Bus = [];
  endproperties
  properties (Access = private)
    handle = [];
  endproperties

  methods
    function this = i2cdev (p, bus, addr, varargin)
      this.Parent = p;
      #this.Pins = pin;

      if nargin < 3
        error ('Expected at least bus and address');
      endif

      this.I2CAddress = addr;

      if ischar(bus)
        # bus will be i2c-1 or i2c-1 etc
        bus = sscanf(bus, "i2c-%d")
      elseif !isnumeric (bus) || bus < 0 || bus > 1
        error ('i2cdev: expected bus to be numeric and 0 or 1');
      endif

      this.Bus = bus;

      this.handle = __i2c__(this.Parent, "open", this.Bus, this.I2CAddress);
      if this.handle < 0
        error ('Couldnt open i2c port');
      endif
    endfunction

    function delete (this)
      try
        p = this.Parent;
        __i2c__(p, "close", this.handle);
	this.Parent = [];
      catch
        # do nothing
      end_try_catch
    endfunction

    function write(this, data, dataPrecision)
      # TODO: the data precision
      __i2c__(this.Parent, "write", this.handle, data);
    endfunction

    function data = read(this, count, dataPrecisiion)
      data = __i2c__(this.Parent, "read", this.handle, count);
    endfunction

    function writeRegister(this, register, data, dataPrecisiion)
      __i2c__(this.Parent, "writeregister", this.handle, register, data);
    endfunction

    function data = readRegister(this, register, cnt, dataPrecisiion)
      data = __i2c__(this.Parent, "readregister", this.handle, register, cnt);
    endfunction

    function disp (this)
      printf("  %s object with properties of:\n", class(this));
      printf("                 Bus = %d\n", this.Bus);
      printf("          I2CAddress = %d (0x%02X)\n", this.I2CAddress, this.I2CAddress);
      #printf("                Pins = %d\n", this.Pins);
    endfunction
  endmethods

endclassdef
